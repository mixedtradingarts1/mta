var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var csrf = require('csurf');
var cors = require('cors');
var passport = require('passport');
require('./passport/passport');
require('./model/db')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var mediaRouter = require('./routes/media');
var uploadRouter = require('./routes/upload');
var tradeRouter = require('./routes/trade');
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true,limit: '1600mb', }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.disable('etag');
if (typeof process.env.NODE_ENV !== 'undefined' && process.env.NODE_ENV == 'production') {
  var whitelist = '.lapits.com';
  var corsOptions = {
      origin: function (origin, callback) {
          if (typeof origin === 'undefined' || origin.search("^.*" + whitelist + ".*$") > -1) {
              callback(null, true)
          } else {
              callback(new Error('Not allowed by CORS'))
          }
      },
      optionsSuccessStatus: 200,
      credentials: true
  }
} else {

  var corsOptions = {
      origin:"https://mixedtradingarts.com",
      //origin:"http://localhost:4213",
      optionsSuccessStatus: 200,
      credentials: true
  }
}
app.use(cors(corsOptions));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/media', mediaRouter);
app.use('/upload', uploadRouter);
app.use('/trade', tradeRouter);
app.use(function(req, res, next) {
  next(createError(404));
});
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
  
});
module.exports = app;
