Users = require('../model/users');
Loginlog = require('../model/logs');
const Sendgrid = require('./sendgrid');
var googleAuth = require('google_authenticator').authenticator;
var macaddress = require('macaddress');
var ga = new googleAuth();
var device = require("device")
var useragent = require("useragent")
var getmac = require("getmac")
var dateFormat = require('dateformat');
var ipAdd = require('ip');

function Function(req) {
    this.req = req;
}

Function.prototype.googleAuthenticator = function (res, callback) {
    callback(ga);
}
Function.prototype.isGoogleAuthActive = function (userid, callback) {
    Security.findOne({ userid: userid, option: 'twofa' }, function (error, res) {
        var check = false;
        if (error) {
            check = false;
        } else {
            if (res !== null && typeof res != 'undefined' && res.secret) {
                check = true;
            } else {
                check = false;
            }
        }
        callback(check);
    });
}
Function.prototype.addLoginLog = async function (userinfo) {
    var userid = userinfo._id;
    var email = userinfo.email;
    var name = userinfo.fname + ' ' + userinfo.lname;
    var agentinfo = this.req.headers['user-agent'];
    // var ip = this.req.headers['x-forwarded-for'] || this.req.connection.remoteAddress;
    var ip = ipAdd.address();
    var agentparse = useragent.parse(agentinfo);
    var agent = agentparse.toAgent();
    var os = agentparse.os.toString();
    var date = Date.now();
    var cdate = dateFormat(date, "yyyy-mm-dd HH:MM:ss");
    var mydevice = device(agentinfo);
    var type = mydevice.type;
    var logData = {
        userid: userid,
        ipaddress: ip,
        agent: agent + ' ' + os,
        description: agentinfo,
        type: type,
        timestamp: cdate,
        browser:agent,
        os:os
    }
    var macAdd = '';
    if (ip !== null && typeof ip !== 'undefined' && ip !== "") {
        await macaddress.one(function (err, mac) {
            logData.macaddress = mac;
            macAdd = mac;
            Loginlog.create(logData);
        });
        var sendGrid = new Sendgrid();
        var options = {
            email: email,
            ip: ip,
            timestamp: logData.timestamp,
            agent: logData.agent,
            name: name,
            type: logData.type,
            mac: logData.macaddress
        };
        sendGrid.sendEmail(
            email,
            'Mixed Trading Arts Login Notification',
            "views/emailtemplate/loginlog.ejs",
            options
        );
    }

}


Function.prototype.verifyDevice = function (user, macaddress, callback) {
    if (user.group !== 'admin') {
        Loginlog.find({ userid: user._id, macaddress: macaddress }).exec(function (error, result) {
            if (error) {
                callback({ error: true, result: error.message })
            } else {
                if (result.length === 0) {
                    callback({ error: false, result: false })
                } else {
                    callback({ error: false, result: true })
                }
            }
        })
    } else {
        callback({ error: false, result: true })
    }
};

module.exports = Function;
