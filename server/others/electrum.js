/**
 * It is library that helps creating addres, transfer amount
 * it supports btc
 */
const RpcClient = require('./rpcClient');
const config = require('config');
const client = new RpcClient(config.get('btc'));

const getBalance = () => {
    return client.execute('getbalance', []);
};

const listAddress = async () => {
    return await client.execute('listaddresses', []);
};

const addRequest = async (amount, memo) => {
	let expiration = 3600;
    let segwit = "--segwit";
    return await client.execute('addrequest', [amount, memo,expiration,segwit]);
};

const getRequest = async (address) => {
    return await client.execute('getrequest', [address]);
};

const listRequest = async () => {
    return await client.execute('listrequests', []);
};

const listUnspent = async () => {
    return await client.execute('listunspent', []);
};

const removeRequest = async (address) => {
    return await client.execute('rmrequest', [address]);
};

const createNewAddress = async () => {
    return await client.execute('createnewaddress', []);
};

const getTranstionDetails = async (address) => {
    return await client.execute('getaddressbalance', [address]);
};


module.exports = {
    getBalance, listAddress, addRequest, listRequest, listUnspent, removeRequest, getRequest, createNewAddress,getTranstionDetails
};
