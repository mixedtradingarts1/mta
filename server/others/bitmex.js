const crypto = require("crypto");
var Request = require("request");
//var bitmexURL = "https://testnet.bitmex.com";
var bitmexURL = "https://www.bitmex.com";
var accountHostServer = "/api/v1";

// -------CLIENT TEST---------
//var apiKey = "flEymoPXC4FZBhkxo4KA-n4E";
//var apiSecret = "AJ1ojQMkF4qMeTOtXXJsYCIUe3FEAkfFuzj6w9ClFuTbEMOZ";

var apiKey = "ag7KJi7kfHawmzB21mkcl_2u";
var apiSecret = "urLCxRmVry312ygEbBn01D79sSIADdg73FlZsICm04oZMZET"; 

//var apiKey = "KNB_e1wDqoSXvWa4Dad8k0j8";
//var apiSecret = "FAqXT2LpuMvlPnKRFInE6TaJMKuR2SvldyVxR1N5KvL08YS_"; 

// -------LIVE CLIENT---------
//var apiKey = "aEFw1J9Cm7MfvOTEsK-orYyi";
//var apiSecret = "qIVNZO7V9zpR0urwgDrDiUh2bMZtrHzNiFgV8d8oy0NvGf1E";


 // var apiKey = "skOu55qohiGGE4i0U_oTyOEW";
 // var apiSecret = "nYcZF_8309SDk4gI3MhGn885XTwXRYxRSDffk2klJsWxnm69";



global.fetch = require('node-fetch');


if (typeof process.env.NODE_ENV !== 'undefined' && process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'production') {
  var frontendURL = process.env.CLIENTURL;
} else {
  var frontendURL = 'https://mixedtradingarts.com/';
}

var bitmexApi = {};

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

bitmexApi.getOrders = function (req, res, next) {

  // var pageIndex = Number(req.query.pageIndex ? req.query.pageIndex : 0);
  // var pageSize = Number(req.query.pageSize ? req.query.pageSize : 0);
  // var pageSkip = Math.abs(pageIndex * pageSize);

  var verb = 'GET';
  var path = accountHostServer + '/order';
  var data = { reverse: true };
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };
   console.log("requestOptions",requestOptions);

  Request(requestOptions, function (error, response, body) {
    console.log("error",error);
    if (error) {
      res.status(500).json({ message: error })
    }
    else {
      if (body) {
        if (!isEmpty(body)) {
          var result = JSON.parse(body);
          var len = result.length;
          res.status(200).json({ data: result, count: len, success: true })
        }
        // console.log("body",body);
        // console.log("JSON.stringify(body)", JSON.parse(body));
      }
    }
  });

}


bitmexApi.getFilledOrders = function (req, res, next) {
  var verb = 'GET';
  var path = accountHostServer + '/execution';
  var data = { reverse: true, "filter": { "execType": ["Trade"] } };
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };
  Request(requestOptions, function (error, response, body) {
    if (error) {
      res.status(500).json({ message: error })
    }
    else {
      if (body) {
        if (!isEmpty(body)) {
          var result = JSON.parse(body);
          var len = result.length;
          res.status(200).json({ data: result, count: len, success: true })
        }
      }
    }
  });

}

bitmexApi.getActiveOrders = function (req, res, next) {
  var verb = 'GET';
  var path = accountHostServer + '/order';
  var data = { reverse: true, "filter": { "open": true } };
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };

  Request(requestOptions, function (error, response, body) {
    if (error) {
      res.status(500).json({ message: error })
    }
    else {
      if (body) {
        if (!isEmpty(body)) {
          var result = JSON.parse(body);
          var len = result.length;

          res.status(200).json({ data: result, count: len, success: true })
        }
      }
    }
  });

}

bitmexApi.getPositions = function (req, res, next) {


  var verb = 'GET';
  var path = accountHostServer + 'position';
  var data = {};
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };

  Request(requestOptions, function (error, response, body) {
    if (error) {
      console.log(error);
      res.status(500).json({ message: error })
    }
    else {
      if (body) {
        if (!isEmpty(body)) {
          var result = JSON.parse(body);
          var len = result.length;
          res.status(200).json({ data: result, count: len, success: true })
        }
      }
    }
  });

}



bitmexApi.getInstrument = function (req, res, next) {
  var verb = 'GET';
  var path = accountHostServer + '/instrument/active';
  var data = {};
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };

  Request(requestOptions, function (error, response, body) {
    if (error) {
      res.status(500).json({ message: error })
    }
    else {
      if (body) {
        if (!isEmpty(body)) {
          var result = JSON.parse(body);


          var len = result.length;
          res.status(200).json({ data: result, count: len, success: true })
        }
      }
    }
  });

}

bitmexApi.hello = function (req, res, next) {
  var verb = 'GET';
  var path = accountHostServer + '/schema/websocketHelp';
  var data = {};
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };

  Request(requestOptions, function (error, response, body) {
    if (error) {
      console.log(error);
      res.status(500).json({ message: error })
    }
    else {
      var result = JSON.parse(body);


      var len = result.length;
      res.status(200).json({ data: result, count: len, success: true })
    }
  });

}


bitmexApi.apiKeyAuthentication = function (apikey, apiSecret, callback) {

  var verb = 'GET';
  var path = accountHostServer + 'user';
  var data = {};
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apikey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };

  Request(requestOptions, function (error, response, body) {

    if (error) {
      callback({ message: error, success: false })
    }
    else {
      if (!isEmpty(body)) {
        var result = JSON.parse(body);
        if (result.id) {
        } else if (result.error.message) {
        }

        callback({ data: result, success: true });
        // res.status(200).json({ data: result, count:len, success: true })
      }
    }
  });

}








































bitmexApi.getOrdersForBoat = function (callback) {

  // var apiKey = "850BqnU2vhoQUupw76JDgWQd";
  // var apiSecret = "6Y6iNwcB44ce-lFZxwidlHK7qGOdHzsrSxiW13qiuTyI3rjp";
  var verb = 'GET';
  var path = accountHostServer + '/order';
  var data = { reverse: true, "filter": { "open": true } };
  var postBody = JSON.stringify(data);
  var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature
  };
  const requestOptions = {
    headers: headers,
    url: bitmexURL + path,
    method: verb,
    body: postBody
  };
  // console.log("requestOptions",requestOptions);
  // console.log('11111111111 return from request');

  Request(requestOptions, function (error, response, body) {
    if (error) {
      console.log(error);
      return callback({ message: error, success: false })
    }
    else {
      // console.log('2222222222222222 return from request');
      if (body) {
        // console.log("isEmpty(body)", isEmpty(body));
        // console.log("!isEmpty(body)", !isEmpty(body));

        if (!isEmpty(body)) {
          // console.log('3333333333333333333 return from request');
          var result = JSON.parse(body);
          var len = result.length;
          // console.log('return from request');
          return callback({ data: result, success: true });
        }
      }
    }
  });

}

var api = module.exports = bitmexApi;

