const crypto = require("crypto");
var Request = require("request");
var bitmexURL = "https://testnet.bitmex.com";
var accountHostServer = "/api/v1/";
var Users = require('../model/users');

// -------CLIENT---------
var apiKey = "";
var apiSecret = "";

// -----------PRIYANSHI--------
// var apiKey = "850BqnU2vhoQUupw76JDgWQd";
// var apiSecret = "6Y6iNwcB44ce-lFZxwidlHK7qGOdHzsrSxiW13qiuTyI3rjp";

global.fetch = require('node-fetch');


if (typeof process.env.NODE_ENV !== 'undefined' && process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'production') {
    var frontendURL = process.env.CLIENTURL;
} else {
    var frontendURL = 'http://client.mixedtradingartslocal.com/';
}

var bitmexUserApi = {};

function isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

bitmexUserApi.getUserOrders = function (req, res, next) {

    try {
        var userid = req.payload._id;

        if (typeof userid != 'undefined' && userid && userid != '') {
            Users.findOne({ _id: userid, group: 'customer' }, { salt: 0, password: 0 }).exec(function (err, resuser) {
                if (err) {
                    res.status(200).json({ message: err, success: false })
                } else {
                    if (resuser !== null) {
                        if (resuser.api_key_status) {
                            apiKey = resuser.api_key_id;
                            apiSecret = resuser.api_key_secret;

                            var verb = 'GET';
                            var path = accountHostServer + 'order';
                            var data = { reverse: true };
                            var postBody = JSON.stringify(data);
                            var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
                            var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
                            var headers = {
                                'content-type': 'application/json',
                                'Accept': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest',
                                // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
                                // https://www.bitmex.com/app/apiKeysUsage for more details.
                                'api-expires': expires,
                                'api-key': apiKey,
                                'api-signature': signature
                            };
                            const requestOptions = {
                                headers: headers,
                                url: bitmexURL + path,
                                method: verb,
                                body: postBody
                            };
                            // console.log("requestOptions",requestOptions);

                            Request(requestOptions, function (error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(200).json({ message: error, success: false })
                                }
                                else {
                                    if (!isEmpty(body)) {
                                    // console.log("body",body);
                                    // console.log("JSON.stringify(body)", JSON.parse(body));
                                    var result = JSON.parse(body);
                                    var len = result.length;
                                    res.status(200).json({ data: result, count: len, success: true })
                                }
                            }
                            });

                        }
                    }
                }
            })
        } else {
            return res.status(400).json({ message: "Unauthrozied Access" })
        }
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }



}


bitmexUserApi.getUserFilledOrders = function (req, res, next) {
    try {
        var userid = req.payload._id;

        if (typeof userid != 'undefined' && userid && userid != '') {
            Users.findOne({ _id: userid, group: 'customer' }, { salt: 0, password: 0 }).exec(function (err, resuser) {
                if (err) {
                    res.status(200).json({ message: err, success: false })
                } else {
                    if (resuser !== null) {
                        if (resuser.api_key_status) {

                            apiKey = resuser.api_key_id;
                            apiSecret = resuser.api_key_secret;
                            var verb = 'GET';
                            var path = accountHostServer + '/execution';
                            var data = { reverse: true, "filter": { "execType": ["Trade"] } };
                            var postBody = JSON.stringify(data);
                            var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
                            var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
                            var headers = {
                                'content-type': 'application/json',
                                'Accept': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest',
                                // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
                                // https://www.bitmex.com/app/apiKeysUsage for more details.
                                'api-expires': expires,
                                'api-key': apiKey,
                                'api-signature': signature
                            };
                            const requestOptions = {
                                headers: headers,
                                url: bitmexURL + path,
                                method: verb,
                                body: postBody
                            };
                            // console.log("requestOptions",requestOptions);

                            Request(requestOptions, function (error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(200).json({ message: error, success: false })
                                }
                                else {
                                    if (!isEmpty(body)) {
                                    // console.log("body",body);
                                    // console.log("getFilledOrders JSON.stringify(body) ", JSON.parse(body));
                                    var result = JSON.parse(body);
                                    var len = result.length;
                                    res.status(200).json({ data: result, count: len, success: true })
                                }
                            }
                            });
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: "Unauthrozied Access" })
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }

}

bitmexUserApi.getUserActiveOrders = function (req, res, next) {

    try {
        var userid = req.payload._id;

        if (typeof userid != 'undefined' && userid && userid != '') {
            Users.findOne({ _id: userid, group: 'customer' }, { salt: 0, password: 0 }).exec(function (err, resuser) {
                if (err) {
                    res.status(200).json({ message: err, success: false })
                } else {
                    if (resuser !== null) {
                        if (resuser.api_key_status) {

                            apiKey = resuser.api_key_id;
                            apiSecret = resuser.api_key_secret;

                            var verb = 'GET';
                            var path = accountHostServer + '/order';
                            var data = { reverse: true, "filter": { "open": true } };
                            var postBody = JSON.stringify(data);
                            var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
                            var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
                            var headers = {
                                'content-type': 'application/json',
                                'Accept': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest',
                                // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
                                // https://www.bitmex.com/app/apiKeysUsage for more details.
                                'api-expires': expires,
                                'api-key': apiKey,
                                'api-signature': signature
                            };
                            const requestOptions = {
                                headers: headers,
                                url: bitmexURL + path,
                                method: verb,
                                body: postBody
                            };
                            // console.log("requestOptions",requestOptions);

                            Request(requestOptions, function (error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(200).json({ message: error, success: false })
                                }
                                else {
                                    if (!isEmpty(body)) {
                                    // console.log("body",body);
                                    // console.log("getActiveOrders JSON.stringify(body) ", JSON.parse(body));
                                    var result = JSON.parse(body);
                                    var len = result.length;
                                    res.status(200).json({ data: result, count: len, success: true })
                                }
                            }
                            });
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: "Unauthrozied Access" })
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }

}

bitmexUserApi.getUserPositions = function (req, res, next) {
    try {
        var userid = req.payload._id;

        if (typeof userid != 'undefined' && userid && userid != '') {
            Users.findOne({ _id: userid, group: 'customer' }, { salt: 0, password: 0 }).exec(function (err, resuser) {
                if (err) {
                    res.status(200).json({ message: err, success: false })
                } else {
                    if (resuser !== null) {
                        if (resuser.api_key_status) {
                            apiKey = resuser.api_key_id;
                            apiSecret = resuser.api_key_secret;

                            var verb = 'GET';
                            var path = accountHostServer + 'position';
                            var data = {};
                            var postBody = JSON.stringify(data);
                            var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
                            var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
                            var headers = {
                                'content-type': 'application/json',
                                'Accept': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest',
                                // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
                                // https://www.bitmex.com/app/apiKeysUsage for more details.
                                'api-expires': expires,
                                'api-key': apiKey,
                                'api-signature': signature
                            };
                            const requestOptions = {
                                headers: headers,
                                url: bitmexURL + path,
                                method: verb,
                                body: postBody
                            };
                            // console.log("requestOptions",requestOptions);

                            Request(requestOptions, function (error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(500).json({ message: error })
                                }
                                else {
                                    if (!isEmpty(body)) {
                                    // console.log("body",body);
                                    // console.log("JSON.stringify(body)", JSON.parse(body));
                                    var result = JSON.parse(body);
                                    var len = result.length;
                                    res.status(200).json({ data: result, count: len, success: true })
                                }
                            }
                            });
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: "Unauthrozied Access" })
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }

}



bitmexUserApi.getUserInstrument = function (req, res, next) {
    try {
        var userid = req.payload._id;

        if (typeof userid != 'undefined' && userid && userid != '') {
            Users.findOne({ _id: userid, group: 'customer' }, { salt: 0, password: 0 }).exec(function (err, resuser) {
                if (err) {
                    res.status(200).json({ message: err, success: false })
                } else {
                    if (resuser !== null) {
                        if (resuser.api_key_status) {
                            apiKey = resuser.api_key_id;
                            apiSecret = resuser.api_key_secret;

                            var verb = 'GET';
                            var path = accountHostServer + '/instrument/active';
                            var data = {};
                            var postBody = JSON.stringify(data);
                            var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
                            var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
                            var headers = {
                                'content-type': 'application/json',
                                'Accept': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest',
                                // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
                                // https://www.bitmex.com/app/apiKeysUsage for more details.
                                'api-expires': expires,
                                'api-key': apiKey,
                                'api-signature': signature
                            };
                            const requestOptions = {
                                headers: headers,
                                url: bitmexURL + path,
                                method: verb,
                                body: postBody
                            };
                            // console.log("requestOptions",requestOptions);

                            Request(requestOptions, function (error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(500).json({ message: error })
                                }
                                else {
                                    if (!isEmpty(body)) {
                                    // console.log("body",body);
                                    // console.log("hello JSON.stringify(body) hello", JSON.parse(body));
                                    var result = JSON.parse(body);

                                    // result.forEach(iitem => {
                                    //   console.log("iitem ----------->>>>>>>>>>>>>>",iitem.rootSymbol );
                                    // })

                                    var len = result.length;
                                    res.status(200).json({ data: result, count: len, success: true })
                                }
                            }
                            });
                        }
                    }
                }
            })
        } else {
            res.status(400).json({ message: "Unauthrozied Access" })
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }

}

bitmexUserApi.hello = function (req, res, next) {
    var verb = 'GET';
    var path = accountHostServer + '/schema/websocketHelp';
    var data = {};
    var postBody = JSON.stringify(data);
    var expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
    var signature = crypto.createHmac('sha256', apiSecret).update(verb + path + expires + postBody).digest('hex');
    var headers = {
        'content-type': 'application/json',
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
        // https://www.bitmex.com/app/apiKeysUsage for more details.
        'api-expires': expires,
        'api-key': apiKey,
        'api-signature': signature
    };
    const requestOptions = {
        headers: headers,
        url: bitmexURL + path,
        method: verb,
        body: postBody
    };
    // console.log("requestOptions",requestOptions);

    Request(requestOptions, function (error, response, body) {
        if (error) {
            console.log(error);
            res.status(500).json({ message: error })
        }
        else {
            // console.log("body",body);
            console.log("hello JSON.stringify(body) hello", JSON.parse(body));
            var result = JSON.parse(body);

            // result.forEach(iitem => {
            //   console.log("iitem ----------->>>>>>>>>>>>>>",iitem.rootSymbol );
            // })

            var len = result.length;
            res.status(200).json({ data: result, count: len, success: true })
        }
    });

}


var api = module.exports = bitmexUserApi;

