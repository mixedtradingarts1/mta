
var SocketModel = {};
var iosocket = {};
var Users = require('../model/users');
const Sendgrid = require('./sendgrid');

SocketModel.socketIoConnection = function (io) {
    if (io) {
        iosocket = io;
        // console.log("iosocket-->",iosocket);
    }
}

SocketModel.TimerUpdateNotification = function (data) {
    if (data) {
        // console.log("data", data);
        // var obj = {};
        // obj = data;
        // console.log("obj", obj);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit("Timer", { "data": data });
            }
        }
    }
}

SocketModel.PaymetSuccess = function (data, id) {
    if (data) {
        // console.log("data ----- PaymetSuccess -----", data, id);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Payment' + id, { "data": data, userid: id });
            }
        }
    }
}

SocketModel.BitmexMargin = function (data) {
    if (data) {
        // console.log("data ----- BitmexMargin -----", data);
        // console.log("iosocket ===>>>>", iosocket);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Margin', { "data": data });
            }
        }
    }
}

SocketModel.BitmexPosition = function (data) {
    if (data) {
        // console.log("data ----- BitmexPosition -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Position', { "data": data });
            }
        }
    }
}

SocketModel.BitmexOrders = function (data) {
    if (data) {
        console.log("data ----- BitmexOrders -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Orders', { "data": data });
                // var Users = require('../model/users');
                // Users.find({ group: 'customer' }).exec(function (err, resuser) {
                //     if (err) {
                //         console.log("error Bitmex Order ==>>", err);
                //     } else {
                //         if (resuser.length > 0) {
                //             resuser.forEach(function (users) {
                //                 var email = users.email;
                //                 data.forEach(function (item) {
                //                     console.log("1111111 item.timestamp", item.timestamp);
                //                     console.log("2222222 new Date()", new Date());

                //                     console.log("33333333333 item.timestamp == new Date()", item.timestamp == new Date());
                //                     var t = new Date();
                //                     // var newTime = t.setSeconds(t.getSeconds() - 20);
                //                     var newTime = new Date(Date.now() - 20000);
                //                     console.log("444444444 newTime", newTime);

                //                     console.log("55555555555 item.timestamp > newTime", item.timestamp > newTime);
                //                     if (item.timestamp == new Date() || item.timestamp > newTime) {
                //                         console.log("hello----->>>>>>>>.. i m in");
                //                         console.log("users.email", users.email);
                //                         console.log("item.text", item.text);
                                        
                //                         var sendGrid = new Sendgrid();
                //                         var options = {
                //                             email: users.email,
                //                             msg: item.text
                //                         };
                //                         sendGrid.sendEmail(
                //                             email,
                //                             'Mixed Trading Arts Notification',
                //                             "views/emailtemplate/bitmexnotification.ejs",
                //                             options
                //                         );
                //                     }
                //                 })
                //             })
                //         }
                //     }
                // })
            }
        }
    }
}

SocketModel.BitmexTrade = function (data) {
    if (data) {
        console.log("data ----- BitmexTrade -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Trade', { "data": data });
            }
        }
    }
}

SocketModel.BitmexInstrument = function (data) {
    if (data) {
        // console.log("data ----- BitmexInstrument -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Instrument', { "data": data });
            }
        }
    }
}


SocketModel.UserProfileUpdate = function (userid, data) {
    if (data) {
        // console.log("data ----- UserProfileUpdate -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Profile' + userid, { "data": data, userid: userid });
            }
        }
    }
}


SocketModel.UpdateContent = function (data) {
    if (data) {
        // console.log("data ----- UpdateContent -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Content', { "data": data });
            }
        }
    }
}

SocketModel.PlanUpdateNotification = function (data) {
    if (data) {
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit("Timer", { "data": data });
            }
        }
    }
}

SocketModel.BitmexMarginUser = function (data, userid) {
    if (data) {
        // console.log("data ----- BitmexMargin -----", data);
        // console.log("iosocket ===>>>>", iosocket);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Margin' + userid, { "data": data });
            }
        }
    }
}

SocketModel.BitmexPositionUser = function (data, userid) {
    if (data) {
        // console.log("data ----- BitmexPosition -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Position' + userid, { "data": data });
            }
        }
    }
}

SocketModel.BitmexOrdersUser = function (data, userid) {
    if (data) {
        // console.log("data ----- BitmexOrders -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Orders' + userid, { "data": data });
            }
        }
    }
}

SocketModel.BitmexInstrumentUser = function (data, userid) {
    if (data) {
        // console.log("data ----- BitmexInstrument -----", data);
        if (typeof iosocket !== 'undefined' && iosocket && iosocket !== null) {
            if (Object.keys(iosocket).length > 0) {
                iosocket.emit('Instrument' + userid, { "data": data });
            }
        }
    }
}

module.exports = SocketModel;
