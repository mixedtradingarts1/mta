/*
'use strict';
const request = require('./request.js');
function RPCClient(options) {
    this.uri = `${options.protocol || 'http'}://${options.host}:${options.port || 18333}${options.path}`;    
    if (!options.auth) {
        this.username = options.username;
        this.password = options.password;
    }
    else {
        let explodes = options.auth.split(":");
        if (explodes.length !== 2) throw new Error('Invalid user name and password.');
        this.username = explodes[0];
        this.password = explodes[1];
    }
    this.id = 0;
}

RPCClient.prototype.execute = async function execute(method, params, wallet="") {
    try{
        var url = this.uri + wallet;
        if(method=="addrequest")
            console.log("Paramater : ",params);
        const res = await request({
            method: 'POST',
            uri: url,
            json: {
                method: method,
                params: params,
                id: this.id++,            
            },
            auth: {
                username: this.username,
                password: this.password
            }
        });

        
        var decoded_data = res.body.toString('utf8');     
        var exeResult = JSON.parse(decoded_data);   
        if (res.statusCode === 401)
            throw new RPCError('Unauthorized (bad API key).', -1);
        if (res.statusCode !== 200)
            throw new Error(`Status code: ${res.statusCode}.`);
        if (!res.body)
            throw new Error('No body for JSON-RPC response.');
        if (res.body.error)
            throw new RPCError(res.body.error.message, res.body.error.code);
        if (exeResult.result == null)
            throw new Error('Result is null');
        return {result:exeResult.result, code:res.statusCode};
    }
    catch(error){
        console.log(error);
    }
};


function RPCError(msg, code) {
    Error.call(this);

    this.type = 'RPCError';
    this.message = String(msg);
    this.code = code >>> 0;

    if (Error.captureStackTrace)
        Error.captureStackTrace(this, RPCError);
}

Object.setPrototypeOf(RPCError.prototype, Error.prototype);
module.exports = RPCClient;
*/


/*!
 * rpcclient.js - json rpc client for bcoin
 * Copyright (c) 2014-2017, Christopher Jeffrey (MIT License).
 * https://github.com/bcoin-org/bcoin
 */

'use strict';
const request = require('./request.js');
function RPCClient(options) {
    this.uri = `${options.protocol || 'http'}://${options.host}:${options.port || 18333}${options.path}`;
    if (!options.auth) {
        this.username = options.username;
        this.password = options.password;
    }
    else {
        let explodes = options.auth.split(":");
        if (explodes.length !== 2) throw new Error('Invalid user name and password.');
        this.username = explodes[0];
        this.password = explodes[1];
    }
    this.id = 0;
}

RPCClient.prototype.execute = async function execute(method, params, wallet="") {
    var url = this.uri + wallet;
    const res = await request({
        method: 'POST',
        uri: url,
        json: {
            method: method,
            params: params,
            id: this.id++
        },
        auth: {
            username: this.username,
            password: this.password
        }
    });
    
    var decoded_data = res.body.toString('utf8');
    var exeResult = JSON.parse(decoded_data);   
   // console.log(res.statusCode);

    if (res.statusCode === 401)
        throw new RPCError('Unauthorized (bad API key).', -1);

    if (res.statusCode !== 200)
        throw new Error(`Status code: ${res.statusCode}.`);

    if (!res.body)
        throw new Error('No body for JSON-RPC response.');

    if (res.body.error)
        throw new RPCError(res.body.error.message, res.body.error.code);

    if (exeResult.result == null){
        throw new Error('Result is null');
        return false;
    }


    return {result:exeResult.result, code:res.statusCode};
};


function RPCError(msg, code) {
    Error.call(this);

    this.type = 'RPCError';
    this.message = String(msg);
    this.code = code >>> 0;

    if (Error.captureStackTrace)
        Error.captureStackTrace(this, RPCError);
}

Object.setPrototypeOf(RPCError.prototype, Error.prototype);

/*
 * Expose
 */

module.exports = RPCClient;