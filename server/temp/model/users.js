var mongoose = require('mongoose');
const socketModel = require('../others/socket');

var UserSchema = new mongoose.Schema({
    newsletter: {
        type: String,
        trim: true,
    },
    payment_type: {
        type: String,
        trim: true,
    },
    referralcode: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    salt: {
        type: String,
        required: true,
    },
    fname: {
        type: String,
        trim: true,
    },
    lname: {
        type: String,
        trim: true,
    },
    phone: {
        type: String,
        trim: true
    },
    address: {
        type: String,
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    state: {
        type: String,
        trim: true
    },
    zipcode: {
        type: String,
        trim: true
    },
    country: {
        type: String,
        trim: true
    },
    email_verified: {
        type: Number,
        required: true,
        default: 0
    },
    phone_verified: {
        type: Number,
        required: true,
        default: 0
    },
    payment_verified: {
        type: Number,
        required: true,
        default: 0
    },
    status: {
        type: String,
        trim: true,
        required: true,
        default: 'Active'
    },
    membership_time: {
        type: Date,
        trim: true,
    },
    expiry_date: {
        type: Date,
        trim: true,
    },
    group: {
        type: String,
        trim: true,
        required: true
    },
    avatar: {
        type: String,
        trim: true,
    },
    api_key_status: {
        type: Boolean,
        trim: true,
        default: false
    },
    api_key_id: {
        type: String,
        trim: true
    },
    api_key_secret: {
        type: String,
        trim: true
    }, 
    installmentPlan: {
        type: String,
        trim: true
    }, 
    numberOfInstallmentPlan: {
        type: Number,
        trim: true
    },
    selectedPlanId: {
        type: String,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

UserSchema.post("findOneAndUpdate", function (doc) {
    var userid = doc._id;
    socketModel.UserProfileUpdate(userid, doc);
});

var Users = mongoose.model('User', UserSchema);
module.exports = Users;
