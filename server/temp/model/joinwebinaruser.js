var mongoose = require('mongoose');

var JoinWebinerUser = new mongoose.Schema({
    email: {
        type: String,
        trim: true,
    },
    name: {
        type: String,
        trim: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})



var JoinWebinerUser = mongoose.model('JoinWebinerUser', JoinWebinerUser);
module.exports = JoinWebinerUser;
