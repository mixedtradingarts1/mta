var mongoose = require('mongoose');
var AdminReplaySchema  = new mongoose.Schema({
    user_id:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true,
    },
    comment_id:{
        type: mongoose.Schema.ObjectId,
        required: true,
        trim: true,
    },
    replay: {
        type: String,
        required: true,
    },
    status: {
        type: Boolean,
        trim:true,
        default:false
    },
    createdAt: {
        type : Date,
        default : Date.now
    }
})
var AdminReplay = mongoose.model('AdminReplay', AdminReplaySchema);
module.exports = AdminReplay;