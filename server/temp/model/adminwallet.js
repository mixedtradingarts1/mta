var mongoose = require('mongoose');
var adminwalletSchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    address: {
        type: String,
        trim: true
    },
    confirmed_balance: {
        type: Number,
        trim: true,
        default:0
    },
    unconfirmed_balance: {
        type: Number,
        trim: true,
        default:0
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
var adminwallet = mongoose.model('adminwallet', adminwalletSchema);
module.exports = adminwallet;