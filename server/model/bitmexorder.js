var mongoose = require('mongoose');
var bitmexOrderSchema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        trim: true
    },
    orderID: {
        type: String,
        trim: true,
        unique: true
    },
    account: {
        type: Number,
        trim: true
    },
    symbol: {
        type: String,
        trim: true
    },
    ordType: {
        type: String,
        trim: true
    },
    ordStatus: {
        type: String,
        trim: true
    },
    workingIndicator: {
        type: Boolean,
        trim: true
    },
    text: {
        type: String,
        trim: true
    },
    timestamp: {
        type: Date,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
var bitmexOrder = mongoose.model('bitmexOrder', bitmexOrderSchema);
module.exports = bitmexOrder;