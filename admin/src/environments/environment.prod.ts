export const environment = {
  production: true,
  apiEndPoint: 'https://kretoss.com:3016/',
  articleImage: 'https://kretoss.com/server/public/uploads/admincontent/imageUpload/',  
};
