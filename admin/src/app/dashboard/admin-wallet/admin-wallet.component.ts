import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-admin-wallet',
  templateUrl: './admin-wallet.component.html',
  styleUrls: ['./admin-wallet.component.css']
})
export class AdminWalletComponent implements OnInit {
  _csrf: '';
  adminWalletData: any;


  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }

   //manage Price form
   walletForm = new FormGroup({
    _csrf: new FormControl('', [Validators.required])
  });

// convenience getter for easy access to form fields
get fcPrice() { return this.walletForm.controls; }


  ngOnInit() {
    this.csrf();
    this.getWalletDetails();

  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.walletForm.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getWalletDetails(){
    this.userService.adminWallet().subscribe((success) => {
      this.adminWalletData = success.data;     
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  addNewAddress(){
    if(confirm("Do you want to generate new address? ")){
      this.userService.newAddress().subscribe((response) => {        
        if (response) {
          if (response.success === true) {
            this.getWalletDetails();
            this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
          }
          else if (response.success === false) {
            this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
          }
        }
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
    }
  }


}
