import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { UploadService } from "../../services/upload.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { environment } from "../../../environments/environment";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService } from '@syncfusion/ej2-angular-richtexteditor';
import { RichTextEditor, Toolbar, Link, Image, HtmlEditor, QuickToolbar } from '@syncfusion/ej2-richtexteditor';
RichTextEditor.Inject(Toolbar, Link, Image, HtmlEditor, QuickToolbar);


@Component({
  selector: 'app-view-content',
  templateUrl: './view-content.component.html',
  styleUrls: ['./view-content.component.css']
})
export class ViewContentComponent implements OnInit {

  textArea: HTMLTextAreaElement;
  mdsource: HTMLElement;
  mdSplit: HTMLElement;
  htmlPreview: HTMLElement;
  defaultRTE: RichTextEditor = new RichTextEditor({});
  selectedVideo: number = 0;
  insertImageSettings: object = {
    saveUrl: `${environment.apiEndPoint}upload/articleImage`,
    path: `${environment.articleImage}`
  };
  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|']
  };

  // public insertImageSettings: object = {
  //   saveUrl: 'http://server.mixedtradingartslocal.com/public/uploads/admincontent/imageUpload/Save',
  //   removeUrl: 'http://server.mixedtradingartslocal.com/public/uploads/admincontent/imageUpload/Remove',
  //   path: 'http://server.mixedtradingartslocal.com/public/uploads/admincontent/imageUpload/Save',
  //   saveFormat:'Base64'
  // };


  _csrf: '';
  appurl = ''
  imgArr: any;
  videoArr: any;
  articleArr: any;
  audioArr: any;

  textflag = false;
  buttonflag = true;

  base64File: string = null;
  filename: string = null;
  base64videoFile: string = null;
  videofilename: string = null;
  indexnum: any;

  base64audioFile: string = null;
  audiofilename: string = null;

  image_file: File = null;
  fileerror = [];
  error = [];
  fileupload = [];

  uploadObj = {
    id: '',
    type: '',
    _csrf: ''
  }

  artObj = {
    article: '',
    id: '',
    type: '',
    _csrf: ''
  }

  audioUrl;
  categoryList = [];
  planList = [];

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
    private uploadService: UploadService,
    protected sanitized: DomSanitizer) {
    if (environment.production) {
      this.appurl = environment.apiEndPoint;
    } else {
      this.appurl = environment.apiEndPoint;
    }
  }

  ngOnInit() {
    this.csrf();
    this.getUploads();
    this.defaultRTE.appendTo('#defaultRTE');
    this.getCategoryList();
    this.getPlanList();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.uploadObj._csrf = success._csrf;
      this.artObj._csrf = success._csrf;
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }
  getCategoryList() {
    this.userService.getCategoryList().subscribe((success) => {
      this.categoryList = success.categoryList;
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  getPlanList(){
    this.userService.getAllPlans().subscribe(success=>{
      this.planList = success.plans;
      console.log(success);
    },error=>{
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getUploads() {
    this.uploadService.adminUploads().subscribe((success) => {


      if (success.status === true) {
        this.imgArr = success.image;
        this.videoArr = success.video;
        this.articleArr = success.article;
        this.audioArr = success.audio;
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  deleteCont(id, type) {
    if (confirm("Do you want to delete this " + type + "?")) {
      this.uploadService.DeleteUpload(id, type).subscribe((response) => {
        if (response) {
          if (response.success === true) {
            this.getUploads();
            this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
          }
          else if (response.success === false) {
            this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
          }
        }
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
    }

  }

  onFileSelect(e: any, index): void {
    try {
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.filename = file.name;
        this.base64File = _event.target.result;
        this.indexnum = index;
      }
      var filelist = e.target.files;
      for (var i = 0; i < filelist.length; i++) {
        if (!this.validateImageFile(filelist[i].name) || (filelist[i].size > 55000000)) {  // file size not exceeding more than 52.45 MB 
          this.error['content_url'] = "";
          this.fileerror['content_url'] = "Invalid File ";
          e.target.value = null;
        }
        else {
          this.fileerror['content_url'] = "";
          this.error['content_url'] = "";
          this.fileupload.push(filelist[i]);
        }
      }
    } catch (error) {
      this.filename = null;
      this.base64File = null;
      console.log('no file was selected...');
    }
  }

  validateImageFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'png') {
      return true;
    }
    else {
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }


  onVideoFileSelect(e: any, index): void {
    console.log("index 444", index);

    try {
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.videofilename = file.name;
        this.base64videoFile = _event.target.result;
        this.indexnum = index;
      }
      var filelist = e.target.files;
      for (var i = 0; i < filelist.length; i++) {
        if (!this.validateVideoUpload(filelist[i].name) || (filelist[i].size > 110000000)) {  // file size not exceeding more than 104.90 MB 
          this.error['content_url'] = "";
          this.fileerror['content_url'] = "Invalid File ";
          e.target.value = null;
        }
        else {
          this.fileerror['content_url'] = "";
          this.error['content_url'] = "";
          this.fileupload.push(filelist[i]);
        }
      }
    } catch (error) {
      this.videofilename = null;
      this.base64videoFile = null;
      console.log('no file was selected...');
    }
  }


  onAudioFileSelect(e: any, index): void {
    try {
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.audiofilename = file.name;
        this.base64audioFile = _event.target.result;
      }
      var filelist = e.target.files;
      if (filelist && filelist[0]) {
        this.indexnum = index;
        this.audioUrl = this.sanitized.bypassSecurityTrustResourceUrl(URL.createObjectURL(filelist[0]));
      }
      for (var i = 0; i < filelist.length; i++) {
        if (!this.validateAudioUpload(filelist[i].name) || (filelist[i].size > 110000000)) {  // file size not exceeding more than 104.90 MB 
          this.error['content_url'] = "";
          this.fileerror['content_url'] = "Invalid File ";
          e.target.value = null;
        }
        else {
          this.fileerror['content_url'] = "";
          this.error['content_url'] = "";
          this.fileupload.push(filelist[i]);
        }
      }
    } catch (error) {
      this.audiofilename = null;
      this.base64audioFile = null;
      console.log('no file was selected...');
    }
  }



  validateVideoUpload(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'mp4' || ext.toLowerCase() == 'mkv' || ext.toLowerCase() == 'webm' || ext.toLowerCase() == 'wmv') {
      return true;
    }
    else {
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }


  validateAudioUpload(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'mp3') {
      return true;
    }
    else {
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }

  updateCont(id, type) {
    if (id !== null && id !== '' && type !== null && type !== '') {
      this.uploadObj.id = id;
      this.uploadObj.type = type;
      // this.uploadService.editAdminUpload(this.uploadObj, this.fileupload).subscribe((success) => {
      //   this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      //   this.getUploads();
      //   this.base64File = null;
      //   this.filename = null;
      //   this.base64videoFile = null;
      //   this.videofilename = null;
      //   this.fileupload = [];
      //   this.csrf();
      // }, (error) => {
      //   this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      // }
      // );
    }
  }

  updateAudioContent(id, type) {
    if (id !== null && id !== '' && type !== null && type !== '') {
      this.uploadObj.id = id;
      this.uploadObj.type = type;
      this.uploadService.editAudioAdminUpload(this.uploadObj, this.fileupload).subscribe((success) => {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getUploads();
        this.base64audioFile = null;
        this.audiofilename = null;
        this.fileupload = [];
        this.csrf();
      }, (error) => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      );
    }
  }

  addArt(art) {
    this.artObj.article = art;
  }

  editIndexArt(index) {
    this.textflag = true;
    this.indexnum = index;
  }

  clear(i) {
    this.textflag = false;
    this.indexnum = i;

  }

  updateArt(id, type) {
    if (id !== null && id !== '' && type !== null && type !== '' &&
      typeof this.artObj.article != 'undefined' && this.artObj.article && this.artObj.article != '') {
      this.artObj.id = id;
      this.artObj.type = type;
      this.uploadService.updateArticle(this.artObj).subscribe((success) => {
        this.textflag = false;
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getUploads();
        this.base64audioFile = null;
        this.audiofilename = null;
        this.fileupload = [];
        this.csrf();
      }, (error) => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      );
    }
  }

  safeHtml(html) {
    return this.sanitized.bypassSecurityTrustHtml(html);
  }


  updateSafeHtml(html) {
    return this.sanitized.bypassSecurityTrustHtml(html);
  }

  changeCategory(category,planName,item,title="") {
    this.userService.changeCategory(item,category,planName,title).subscribe((response) => {
      if (response) {
        if (response.success === true) {
          this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        }
        else if (response.success === false) {
          this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }
  inArray(value: string, array: Array<string>): boolean {
    return array.includes(value);
  }

  editVideo(index: number): void {
    this.selectedVideo = index;
  }
  videoUpdateCancel(): void {
    this.selectedVideo = 0;
  }
  updateVideo(id,videoTitle,videoURL): void {
    console.log( this.uploadObj);
    if (id !== null && id !== '' && videoTitle !== null && videoURL !== '') {      
      this.uploadService.editAdminUpload(id,videoTitle,videoURL,this.uploadObj).subscribe((success) => {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getUploads();
        this.base64audioFile = null;
        this.audiofilename = null;
        this.fileupload = [];
        this.selectedVideo = 0;
        this.csrf();
      }, (error) => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      );
    }
  }
}
