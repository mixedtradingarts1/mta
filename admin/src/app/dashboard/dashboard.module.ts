import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomMaterialModule } from '../material.module';
import { LayoutComponent } from './layout/layout.component';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ManageCustomerlistComponent } from './manage-customerlist/manage-customerlist.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { MembershipPlanComponent } from './membership-plan/membership-plan.component';
import { ClockSettingComponent } from './clock-setting/clock-setting.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { UploadContentComponent } from './upload-content/upload-content.component';
import { ViewContentComponent } from './view-content/view-content.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { AdminWalletComponent } from './admin-wallet/admin-wallet.component';
import { QRCodeModule } from 'angularx-qrcode';
import { CategoryComponent } from './category/category.component';
import { DialogComponent } from './dialog/dialog.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { SafePipe } from 'src/app/pipe/safe.pipe';
import { ContentDetailsComponent } from './content-details/content-details.component';
import { JoinwebineruserComponent } from './joinwebineruser/joinwebineruser.component';

@NgModule({
  declarations: [
    LayoutComponent,
    DashboardComponent,
    ManageCustomerlistComponent,
    CustomerDetailsComponent,
    MembershipPlanComponent,
    ClockSettingComponent,
    UploadContentComponent,
    ViewContentComponent,
    AdminWalletComponent,
    CategoryComponent,
    DialogComponent,
    SubCategoryComponent,
    SafePipe,   
    ContentDetailsComponent, JoinwebineruserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomMaterialModule,
    DashboardRoutingModule,
    NgxMaterialTimepickerModule,
    RichTextEditorAllModule,
    QRCodeModule
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class DashboardModule { }
