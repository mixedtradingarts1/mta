import { Component, OnInit, ElementRef, Output, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { AdminService } from "../../services/admin.service";
import { UploadService } from "../../services/upload.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { environment } from "../../../environments/environment";
import { DomSanitizer } from '@angular/platform-browser';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService } from '@syncfusion/ej2-angular-richtexteditor';
import { RichTextEditor, Toolbar, Link, Image, HtmlEditor, QuickToolbar } from '@syncfusion/ej2-richtexteditor';
import { HttpClient } from '@angular/common/http';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
RichTextEditor.Inject(Toolbar, Link, Image, HtmlEditor, QuickToolbar);


@Component({
  selector: 'app-upload-content',
  templateUrl: './upload-content.component.html',
  styleUrls: ['./upload-content.component.css'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService]
})
export class UploadContentComponent implements OnInit {
  textArea: HTMLTextAreaElement;
  mdsource: HTMLElement;
  mdSplit: HTMLElement;
  htmlPreview: HTMLElement;
  defaultRTE: RichTextEditor = new RichTextEditor({});
  insertImageSettings: object = {
    saveUrl: `${environment.apiEndPoint}upload/articleImage`,
    path: `${environment.articleImage}`,
  };
  public tools: object = {
    type: 'MultiRow',
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|']
  };

  // public insertImageSettings: object = {
  //   saveUrl: 'http://server.mixedtradingartslocal.com/public/uploads/admincontent/articleImageUpload/'
  // };

  public onToolbarClick(e: any): void {
    console.log("onToolbarClick --- e", e);

  }


  public onImageUploadSuccess(e: any): void {
    console.log("onImageUploadSuccess ---- e", e);

  }

  public imageSelected(e: any): void {
    console.log("imageSelected ---->>>> e", e);

  }

  public imageRemoving(e: any): void {
    console.log("imageRemoving ", e)
  }


  @ViewChild('selectvideofile') video_el: ElementRef;   //in html we make variable of selectfile
  @ViewChild('selectaudiofile') audio_el: ElementRef;
  @ViewChild('fileInput') el: ElementRef;
  @ViewChild('video') videl: ElementRef;


  imageUrl: any = 'assets/images/icons/noimage.png';
  editFile: boolean = true;
  removeUpload: boolean = false;

  imageUrlAud: any = 'assets/images/icons/noimage.png';
  editAudioFile: boolean = true;
  removeAudioUpload: boolean = false;

  imageUrlArt: any = 'assets/images/icons/noimage.png';
  editArticleFile: boolean = true;
  removeArticleUpload: boolean = false;

  _csrf: '';
  base64File: string = null;
  filename: string = null;
  base64videoFile: string = null;
  videofilename: string = null;
  audiofilename: string = null;

  appurl = ''

  image_file: File = null;
  fileerror = [];
  error = [];
  fileupload = [];


  videoUrl;
  audioUrl;
  videoLength: number;
  audioLength: number;
  validations: any;
  uploadPercent: any;
  progress = { loaded: 0, total: 0 };
  audiouploadPercent: any;
  audioProgress = { loaded: 0, total: 0 };

  flagUpload: boolean;
  duration: string;
  videoFilesArray = [];
  AllFiledata: any;
  AudioFiledata: any;

  value: any;

  dynamicFile = {
    media_name: {},
    media_path: {},
    torrent_id: {},
    type: {},
    userid: {},
    extension: {},
    fileSize: {},
    torrentarr: [],
    description: {},
    thumbnail: {},
    duration: {}
  }

  bundleObj = {
    title: '',
    description: '',
    _csrf: '',
    fileFlag: false,
    code: ''
  }

  videoObj = {
    privacy: null,
    title: '',
    _csrf: ''
  }

  audioObj = {
    privacy: null,
    title: '',
    _csrf: ''
  }

  selectedRadio: String;
  selectedRadioAudio: String;

  constructor(private router: Router,
    private userService: UserService,
    public apiService: ApiService,
    public adminService: AdminService,
    public uploadService: UploadService,
    private snack: MatSnackBar,
    private sanitizer: DomSanitizer,
    private http: HttpClient,
    private cd: ChangeDetectorRef
  ) {
    if (environment.production) {
      this.appurl = environment.apiEndPoint;
    } else {
      this.appurl = environment.apiEndPoint;
    }
  }

  ArticleForm = new FormGroup({
    article: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required]),
    thumbnail: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

  ImageForm = new FormGroup({
    file: new FormControl('', [Validators.required]),
    filename: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required]),
  });

  VideoForm = new FormGroup({
    videofilename: new FormControl('', [Validators.required]),
    radiobtn: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required])    
  });

  AudioForm = new FormGroup({
    audiofilename: new FormControl('', [Validators.required]),
    radiobtn: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required]),
    thumbnail: new FormControl('', [Validators.required])
  });

  // convenience getter for easy access to form fields
  get articlefc() { return this.ArticleForm.controls; }

  get imagefc() { return this.ImageForm.controls; }

  get videofc() { return this.VideoForm.controls; }

  get audiofc() { return this.AudioForm.controls; }


  ngOnInit() {
    this.csrf();
    this.innerImg();
    this.getMediaValidations();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.bundleObj._csrf = success._csrf;
      this.ArticleForm.patchValue({ _csrf: this._csrf });
      this.videoObj._csrf = success._csrf;
      this.audioObj._csrf = success._csrf;
      console.log(" this._csrf", this._csrf);
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  innerImg() {
    // this.apiService.get
    this.apiService.gtcsrf().subscribe((success) => {


      // this.http.post(`${this.appurl}upload/articleImage?_csrf=${this._csrf}`, this.insertImageSettings, {headers: {Authorization: `Bearer ${this.apiService.getToken()}`}});

      // console.log("  this.insertImageSettings",  this.insertImageSettings);

    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  onFileSelect(e: any): void {
    try {
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.filename = file.name;
        this.base64File = _event.target.result;
      }
      var filelist = e.target.files;
      console.log(filelist);

      for (var i = 0; i < filelist.length; i++) {
        if (!this.validateImageFile(filelist[i].name) || (filelist[i].size > 55000000)) {  // file size not exceeding more than 52.45 MB 
          this.error['content_url'] = "";
          this.fileerror['content_url'] = "Invalid File ";
          e.target.value = null;
        }
        else {
          this.fileerror['content_url'] = "";
          this.error['content_url'] = "";
          this.fileupload.push(filelist[i]);
          console.log("this.fileupload 333333333", this.fileupload);
        }
      }
    } catch (error) {
      this.filename = null;
      this.base64File = null;
      console.log('no file was selected...');
    }
  }

  uploadFile(event) {
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.fileupload.push(file);
        this.editFile = false;
        this.removeUpload = true;
      }
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }

  // Function to remove uploaded file
  removeUploadedFile() {
    let newFileList = Array.from(this.el.nativeElement.files);
    this.imageUrl = 'assets/images/icons/noimage.png';
    this.editFile = true;
    this.removeUpload = false;

  }

  uploadFileAudioImg(event) {
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrlAud = reader.result;
        this.fileupload.push(file);
        this.editAudioFile = false;
        this.removeAudioUpload = true;
      }
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }


  uploadFileArticleImg(event) {
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrlArt = reader.result;
        this.fileupload.push(file);
        this.editArticleFile = false;
        this.removeArticleUpload = true;
      }
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }

  // Function to remove uploaded file
  removeUploadedFileAudioImg() {
    let newFileList = Array.from(this.el.nativeElement.files);
    this.imageUrlAud = 'assets/images/icons/noimage.png';
    this.editAudioFile = true;
    this.removeAudioUpload = false;

  }

  // Function to remove uploaded file
  removeUploadedFileArticleImg() {
    let newFileList = Array.from(this.el.nativeElement.files);
    this.imageUrlArt = 'assets/images/icons/noimage.png';
    this.editArticleFile = true;
    this.removeArticleUpload = false;

  }

  resetArticle(){
    this.fileupload = []; 
    this.imageUrlArt = 'assets/images/icons/noimage.png';
    this.editArticleFile = true;
    this.csrf();
  }

  readVideoUrl(event: any) {
    try {
      const file = event.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.videofilename = file.name;
        this.base64videoFile = _event.target.result;
      }
      const files = event.target.files;
      if (files && files[0]) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(files[0]));
      }
      if (event.target.files !== null && typeof event.target.files !== undefined) {
        var filelist = event.target.files;
        for (var i = 0; i < filelist.length; i++) {
          if (!this.validateVideoUpload(filelist[i].name) || (filelist[i].size > 110000000)) {  // file size not exceeding more than 104.90 MB 
            this.error['content_url'] = "";
            this.fileerror['content_url'] = "Invalid File ";
            event.target.value = null;
          }
          else {
            // this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
            this.fileerror['content_url'] = "";
            this.error['content_url'] = "";
            this.fileupload.push(filelist[i]);
          }
        }
      }
    } catch (error) {
      this.videofilename = null;
      this.base64videoFile = null;
      console.log('no file was selected...');
    }
  }


  readAudioUrl(event: any) {
    // const files = event.target.files;
    // if (files && files[0]) {
    //   this.audioUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(files[0]));
    //   console.log(this.audioUrl);
    // }

    try {
      console.log("Demo");
      const file = event.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.audiofilename = file.name;
        this.base64videoFile = _event.target.result;
      }
      const files = event.target.files;
      if (files && files[0]) {
        this.audioUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(files[0]));

      }
      if (event.target.files !== null && typeof event.target.files !== undefined) {
        var filelist = event.target.files;
        for (var i = 0; i < filelist.length; i++) {
          console.log(filelist[i].size);
          if (!this.validateAudioUpload(filelist[i].name) || (filelist[i].size > 1000000000)) {  // file size not exceeding more than 104.90 MB 
            console.log("?Demo");
            this.error['content_url'] = "";
            this.fileerror['content_url'] = "Invalid File ";
            event.target.value = null;
          }
          else {
            this.fileerror['content_url'] = "";
            this.error['content_url'] = "";
            this.fileupload.push(filelist[i]);
          }
        }
      }
    } catch (error) {
      this.audiofilename = null;
      this.base64videoFile = null;
      console.log('no file was selected...');
    }
  }

  // Method to validate upload file by its duration
  getDuration(e) {
    const duration = e.target.duration;
    console.log("duration", duration);
    if (duration <= this.validations.video_duration) {

      this.videoLength = duration;
      this.uploadVideoFile(File);
    } else {
      this.snack.open('File duration should be less than', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      this.videoLength = null;
      return false
    }
  }

  // Method to validate upload file by its duration
  setDuration(load_event): void {
    const duration = Math.round(load_event.currentTarget.duration);
    if (duration <= this.validations.audio_duration) {
      this.audioLength = duration;
      this.uploadAudioFile(File);
    } else {
      this.snack.open('File duration should be less than ' + Math.floor(this.validations.audio_duration / 60) + ' Minutes', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      this.audioLength = null;
    }
  }

  // Method to fetch validations on content from server
  getMediaValidations() {
    this.uploadService.getValidations().subscribe((validations) => {
      console.log("validations", validations);
      this.validations = validations;
    }, (error) => {
      console.log(error.error.message);
    })
  }

  validateVideoUpload(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'mp4' || ext.toLowerCase() == 'mkv' || ext.toLowerCase() == 'webm' || ext.toLowerCase() == 'wmv') {
      return true;
    }
    else {
      this.VideoForm.controls.videofilename.reset();
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }

  validateAudioUpload(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'mp3') {
      return true;
    }
    else {
      this.AudioForm.controls.audiofilename.reset();
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }

  validateImageFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'png') {
      return true;
    }
    else {
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }


  uploadVideoFile = (file) => {
    var i = 0;
    console.log("videl", this.videl);

    if (typeof this.video_el.nativeElement.files !== 'undefined' && this.video_el.nativeElement.files.length > 0) {
      var filedata = this.video_el.nativeElement.files[0];
      if (typeof filedata !== 'undefined' && this.validateVideoUpload(filedata.name) && (filedata.size <= this.validations.video_size) && (this.videoLength <= this.validations.video_duration)) {   // file size to upload validating by admin
        this.uploadService.uploadFileData(this.appurl + 'upload/videoupload?_csrf=' + this._csrf, filedata)
          .subscribe(
            (data: any) => {
              if (data.type == 1 && data.loaded && data.total) {
                this.progress.loaded = data.loaded;
                this.progress.total = data.total;
                this.uploadPercent = ((Number(data.loaded) / Number(data.total) * 100).toFixed(2))
                if (this.uploadPercent == 100) {
                  this.AllFiledata = filedata;
                  this.snack.open('Video File Ready to upload', 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
                }
              }
            },
            error => {
              this.snack.open(error.error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
            }
          )
      } else {
        console.log("filedata 11111111", filedata);
        if (typeof filedata !== 'undefined' && !(filedata.size <= this.validations.video_size)) {
          console.log("filedata 222222222222222", filedata);
          this.snack.open('Upload size not exceeding ' + this.validations.video_size / (1000 * 1000), 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      }
    }
  }


  uploadAudioFile = (file) => {
    var i = 0;
    var filedata = this.audio_el.nativeElement.files[0];
    if (typeof filedata !== 'undefined' && this.validateAudioUpload(filedata.name) && (filedata.size <= this.validations.audio_size) && (this.audioLength <= this.validations.audio_duration)) {   // file size to upload validating by admin
      this.uploadService.uploadFileData(this.appurl + 'upload/audioupload?_csrf=' + this._csrf, filedata)
        .subscribe(
          (data: any) => {
            if (data.type == 1 && data.loaded && data.total) {
              this.audioProgress.loaded = data.loaded;
              this.audioProgress.total = data.total;
              this.audiouploadPercent = ((Number(data.loaded) / Number(data.total) * 100).toFixed(2))
              if (this.audiouploadPercent == 100) {
                this.AudioFiledata = filedata;
                this.snack.open('Video File Ready to upload', 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
              }
            }

          },
          error => {
            this.snack.open(error.error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
          }
        )
    } else {
      if (typeof filedata !== 'undefined' && !(filedata.size <= this.validations.audio_size)) {
        this.snack.open('Upload size not exceeding ' + this.validations.audio_size / (1000 * 1000), 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }
  }

  // Method to convert seconds to MM:SS format
  durationSecToMMSS(sec) { return (sec - (sec %= 60)) / 60 + (9 < sec ? ':' : ':0') + sec }

  uploadContent(form: NgForm) {
    this.videoObj.title = this.VideoForm.value.title;
    this.uploadService.adminUpload(this.VideoForm.value, this.videoObj).subscribe((success) => {
      this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      form.reset();   
      this.base64File = null;
      this.filename = null;
      this.fileupload = [];
      this.uploadPercent = null;
      this.imageUrl = 'assets/images/icons/noimage.png';
      this.editFile = true;
      this.csrf();
    },
      (error) => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    );
  }
  
  uploadsImage(form: NgForm) {
    this.videoObj.title = this.ImageForm.get("title").value;
    console.log(this.videoObj);
    this.uploadService.adminUploadsImage(this.fileupload, this.videoObj).subscribe((success) => {
      this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      form.reset();
      // this.VideoForm.
      this.base64File = null;
      this.filename = null;
      this.fileupload = [];
      this.uploadPercent = null;
      this.imageUrl = 'assets/images/icons/noimage.png';
      this.editFile = true;
      this.csrf();
    },
      (error) => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    );
  }



  uploadAudioContent(form: NgForm) {
    this.audioObj.title = this.AudioForm.value.title;
    this.uploadService.adminAudioUpload(this.fileupload, this.audioObj).subscribe((success) => {
      this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      form.reset();
      // this.VideoForm.
      this.base64File = null;
      this.filename = null;
      this.fileupload = [];
      this.audiouploadPercent = null;
      this.imageUrlAud = '../../../assets/images/icons/noimage.png';
      this.editAudioFile = true;
      this.csrf();
    },
      (error) => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    );
  }

  radioChange(e) {
    this.videoObj.privacy = this.selectedRadio;
  }

  audradioChange(e) {
    this.audioObj.privacy = this.selectedRadioAudio;
  }


  addArticle() {
    // stop here if form is invalid
    // if (this.ArticleForm.invalid) {
    //   return false;
    // }
    this.defaultRTE.appendTo('#defaultRTE');
    // this.textArea = this.defaultRTE.contentModule.getEditPanel() as HTMLTextAreaElement;
    // console.log("this.textArea", this.textArea);
    // console.log("this.textArea.innerHTML", this.textArea.innerHTML);


    // console.log(" this.defaultRTE.getText", this.defaultRTE.getText());
    // // console.log(" this.defaultRTE.innerHTML",this.defaultRTE.sanitizeHtml() );
    // var abc;
    // var xyz;

    // form.value.article.innerHTML = abc.getValue();

    // console.log("form.value.article", form.value.article.innerHTML);

    console.log("this.ArticleForm.value", this.ArticleForm.value);

    this.uploadService.uploadArticle(this.fileupload, this.ArticleForm.value).subscribe((data: any) => {

      console.log("data -->>", data);

      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        // form.reset();
        // this.ArticleForm.controls.article.

        this.fileupload = []; 
        this.imageUrlArt = '../../../assets/images/icons/noimage.png';
        this.editArticleFile = true;
        this.csrf();
        this.ArticleForm.controls.article.reset();
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }
      , error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
  }




}
