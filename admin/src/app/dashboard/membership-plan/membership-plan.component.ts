import { Component, OnInit, ElementRef, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MatTable, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-membership-plan',
  templateUrl: './membership-plan.component.html',
  styleUrls: ['./membership-plan.component.css']
})
export class MembershipPlanComponent implements OnInit {

  _csrf: '';
  btcPrice: any;
  checking = false;

  displayedColumns = ['plan', 'amount', 'installmentPlan', 'action'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatTable) table: MatTable<any>;


  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }


  // membership plan form
  memberForm = new FormGroup({
    num: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
    installmentPlan: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
    time: new FormControl('', [Validators.required]),
    amount: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$')]),
    lifetime: new FormControl(''),
    _csrf: new FormControl('', [Validators.required])
  });

  //manage Price form
  PriceForm = new FormGroup({
    amount: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$')]),
    _csrf: new FormControl('', [Validators.required])
  });

  // convenience getter for easy access to form fields
  get fc() { return this.memberForm.controls; }

  get fcPrice() { return this.PriceForm.controls; }


  ngOnInit() {
    this.csrf();
    this.getAllPlans();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.memberForm.patchValue({ _csrf: this._csrf });
      this.PriceForm.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  addPlans() {
    // stop here if form is invalid
    if (this.memberForm.invalid) {
      return false;
    }
    this.userService.addplans(this.memberForm.value).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.getAllPlans();
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        // this.memberForm.reset();
        this.memberForm.controls.installmentPlan.reset();
        this.memberForm.controls.num.reset();
        this.memberForm.controls.time.reset();
        this.memberForm.controls.amount.reset();
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // addBTCPrice(){
  //   // stop here if form is invalid
  //   if (this.PriceForm.invalid) {
  //     return false;
  //   }
  //   this.userService.addBtcPrice(this.PriceForm.value).subscribe((data: any) => {
  //     if (data.success === false) {
  //       this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
  //     }
  //     if (data.success === true) {
  //       this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
  //       this.PriceForm.controls.amount.reset();
  //       this.getBtcPrice();
  //     }
  //     if (data[0]) {
  //       this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
  //     }
  //   }, error => {
  //     this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
  //   });
  // }

  getAllPlans() {
    this.userService.getAllPlans().subscribe((success) => {
      if (success.status === true) {
        this.dataSource.data = success.plans;
        console.log(this.dataSource.data);
      } 
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  DeletePlans(id, plan) {
    if (confirm("Do you want to delete this plan? ")) {
      this.userService.deletePlans(id, plan).subscribe((response) => {
        // if (response) {
        if (response.success === true) {
          this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
          this.getAllPlans();
        }
        else if (response.success === false) {
          this.snack.open(response.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
        // }
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
    }
  }

  showOptions(value) {
    console.log(value);
    if (value) {
      this.memberForm.controls.num.disable();
      this.memberForm.controls.time.disable();
      this.memberForm.patchValue({ lifetime: 'Lifetime Membership' });
    }
    if (!value) {
      this.memberForm.controls.num.enable();
      this.memberForm.controls.time.enable();
    }
  }

}
