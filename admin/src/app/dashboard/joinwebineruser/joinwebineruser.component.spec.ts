import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinwebineruserComponent } from './joinwebineruser.component';

describe('JoinwebineruserComponent', () => {
  let component: JoinwebineruserComponent;
  let fixture: ComponentFixture<JoinwebineruserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinwebineruserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinwebineruserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
