import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MatTableDataSource, MatPaginator, MatSnackBar, PageEvent } from '@angular/material';

@Component({
  selector: 'app-joinwebineruser',
  templateUrl: './joinwebineruser.component.html',
  styleUrls: ['./joinwebineruser.component.css']
})
export class JoinwebineruserComponent implements OnInit {

  users: any;
  displayedColumns: string[] = ['firstname','email','registrationDate'];
  dataSource = new MatTableDataSource();

  pageIndex = 0;
  pageLimit = [5, 10, 15];
  limitUser = 10;
  totalUserLength = 0;
  search = undefined;
  show = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.gettingAllUsersDetails();
  }


  gettingAllUsersDetails() {
    this.userService.getJoinWebinerUserList(this.pageIndex, this.limitUser, this.search).subscribe((users) => {           
      if (users) {
        this.dataSource.data = users.users;
        this.totalUserLength = users.count;
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // searching in Users
  searchUsers(value: any) {
    this.search = value;
    this.gettingAllUsersDetails();
  }
  // chnage limit and previous next button in Users
  getAllUsers(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.limitUser = event.pageSize;
    this.gettingAllUsersDetails();
  }

  
}
