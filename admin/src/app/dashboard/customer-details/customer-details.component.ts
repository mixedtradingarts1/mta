import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import {
  MatTabChangeEvent,
  MatTableDataSource,
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
  MatSnackBar,
  PageEvent,
  MatSlideToggle
} from '@angular/material';
@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {
  userId: string;
  profileStatus: string;
  firstName: string;
  lastName: string;
  email: string;
  address: string;
  city: string;
  country: string;
  zipCode: string;
  state: string;
  profilePicture: any;
  isemail: any;
  isphone: any;
  phone: any;
  newsletter: string;
  payment_type: string;
  refCode: any;
  payment_date: any;
  avtar: any;
  btcAddress: string;
  btcAmount: any;
  paymentDate: any;
  invoceId: any;
  memo: any;
  paymentStatus: string;
  ispaid: any;
  expiryDate: any;
  Noexpiry: any;

  constructor(private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.userId = this.activatedRoute.snapshot.params.id;

    if (this.userId) {
      this.getUserBasicInfo(this.userId);
      this.getPaymetHistory(this.userId);

    }
  }


  // getting user and user profile infor
  getUserBasicInfo(userId) {
    this.userService.getSingleUserInfo(userId).subscribe((userProfile) => {
      console.log("userProfile.user", userProfile.user);

      if (userProfile) {
        if (userProfile.user.status === 'Active') {
          this.profileStatus = userProfile.user.status;
        }
        if (userProfile.user.status === 'Suspended') {
          this.profileStatus = userProfile.user.status;
        }
        this.firstName = userProfile.user.fname;
        this.lastName = userProfile.user.lname;
        this.email = userProfile.user.email;
        this.isemail = userProfile.user.email_verified;
        this.phone = userProfile.user.phone;
        this.isphone = userProfile.user.phone_verified;
        this.newsletter = userProfile.user.newsletter;
        this.payment_type = userProfile.user.payment_type;
        this.refCode = userProfile.user.referralcode;
        this.address = userProfile.user.address;
        this.city = userProfile.user.city;
        this.state = userProfile.user.state;
        this.zipCode = userProfile.user.zipcode;
        this.country = userProfile.user.country;
        this.avtar = userProfile.user.avatar;

        console.log(this.avtar, typeof this.avtar);
        if ( typeof userProfile.user.avatar === 'undefined') {
          this.profilePicture = true;
        }

        if (userProfile.user.membership_time && userProfile.user.membership_time !== null) {
          this.payment_date = userProfile.user.membership_time;
        }

        if (userProfile.user.newsletter && userProfile.user.newsletter !== null && userProfile.user.newsletter === 'Lifetime Membership') {
          this.Noexpiry = 'Lifetime Member';
        } else if (userProfile.user.newsletter && userProfile.user.newsletter !== null && userProfile.user.newsletter !== 'Lifetime Membership') {
          this.expiryDate = userProfile.user.expiry_date;
        }
      }
    });
  }


  getPaymetHistory(userId) {
    this.userService.getPaymentDetails(userId).subscribe((data) => {
      if (data) {
        if (data.payment.status === 'Pending') {
          this.ispaid = data.payment.status;
        }
        if (data.payment.status === 'Paid') {
          this.ispaid = data.payment.status;
        }

        this.btcAddress = data.payment.address;
        this.btcAmount = data.payment.btcamount;
        this.invoceId = data.payment.invoiceId;
        this.memo = data.payment.memo;
        this.paymentStatus = data.payment.status;


      }
    });
  }

}
