import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { Injectable, ElementRef, ViewChild } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { UserService } from "../../services/user.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MatSelectModule } from '@angular/material';

/**
 * Node for to-do item
 */
export class TodoItemNode {
  children: TodoItemNode[];
  item: string;
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

// /**
//  * The Json object for to-do list data.
//  */
// const TREE_DATA = {
//   // Groceries: {
//   //   'Almond Meal flour': null,
//   //   'Organic eggs': null,
//   //   'Protein Powder': null,
//   //   Fruits: {
//   //     Apple: null,
//   //     Berries: ['Blueberry', 'Raspberry'],
//   //     Orange: null
//   //   }
//   // },
//   Reminders: [
//     'Cook dinner',
//     'Read the Material Design spec',
//     'Upgrade Application to Angular'
//   ],
//   alarm: [],
//   hello: [],
//   music: [],
//   entertainment:[]
// };

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }


  // constructor() {
  //   this.initialize();
  // }

  // initialize() {
  //   // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
  //   //     file node as children.
  //   const data = this.buildFileTree(TREE_DATA, 0);

  //   // Notify the change.
  //   this.dataChange.next(data);
  // }

  // /**
  //  * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
  //  * The return value is the list of `TodoItemNode`.
  //  */
  // buildFileTree(obj: object, level: number): TodoItemNode[] {
  //   return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
  //     const value = obj[key];
  //     const node = new TodoItemNode();
  //     node.item = key;

  //     if (value != null) {
  //       if (typeof value === 'object') {
  //         node.children = this.buildFileTree(value, level + 1);
  //       } else {
  //         node.item = value;
  //       }
  //     }

  //     return accumulator.concat(node);
  //   }, []);
  // }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string) {
    if (parent.children) {
      parent.children.push({ item: name } as TodoItemNode);
      this.dataChange.next(this.data);
    }
  }

  // updateItem(node: TodoItemNode, name: string) {
  //   // console.log("updateItem -->>node,name ", node,name);

  //   node.item = name;
  //   // console.log("this.data", this.data);

  //   this.dataChange.next(this.data);
  // }
}

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [ChecklistDatabase]
})
export class CategoryComponent implements OnInit {

  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  TREE_DATA: any;
  treedatasource: any;
  newCat = false;
  catObj = {
    cat_name: '',
    subcat_name: ''
  }
  parentNodeData: any;
  categoryIdList:any;

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: TodoItemFlatNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<TodoItemFlatNode>;

  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  constructor(
    private database: ChecklistDatabase,
    private userService: UserService, private snack: MatSnackBar, ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    // database.dataChange.subscribe(data => {
    //   this.dataSource.data = data;
    //   console.log("this.dataSource", this.dataSource);

    // });
  }

  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;

  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.item
      ? existingNode
      : new TodoItemFlatNode();
    flatNode.item = node.item;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  ngOnInit() {
    this.getCategories();

  }

  initialize(treedata) {
    console.log(treedata);
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    this.treedatasource = this.buildFileTree(treedata, 0);

    this.database.dataChange.subscribe(data => {
      this.dataSource.data = this.treedatasource;
      console.log("data",treedata);
    });
    // this.dataSource.data = this.treedatasource;
    // Notify the change.
    console.log(this.treedatasource);
    this.dataChange.next(this.treedatasource);
  }

  buildFileTree(obj: object, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }
  /** Whether all the descendants of the node are selected */
  // descendantsAllSelected(node: TodoItemFlatNode): boolean {
  //   const descendants = this.treeControl.getDescendants(node);
  //   return descendants.every(child => this.checklistSelection.isSelected(child));
  // }

  /** Whether part of the descendants are selected */
  // descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
  //   const descendants = this.treeControl.getDescendants(node);
  //   const result = descendants.some(child => this.checklistSelection.isSelected(child));
  //   return result && !this.descendantsAllSelected(node);
  // }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  // todoItemSelectionToggle(node: TodoItemFlatNode): void {
  //   this.checklistSelection.toggle(node);
  //   const descendants = this.treeControl.getDescendants(node);
  //   this.checklistSelection.isSelected(node)
  //     ? this.checklistSelection.select(...descendants)
  //     : this.checklistSelection.deselect(...descendants);
  // }


/** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
  this.checklistSelection.toggle(node);
  // console.log("todoLeafItemSelectionToggle node", node);
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    // console.log("parent", parent);   
    // console.log("this.checklistSelection.isSelected(node)", this.checklistSelection.isSelected(node));   
    this.deleteItem(node, parent);
}



/* Get the parent node of a node */
getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
  const currentLevel = this.getLevel(node);

  if (currentLevel < 1) {
    return null;
  }

  const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

  for (let i = startIndex; i >= 0; i--) {
    const currentNode = this.treeControl.dataNodes[i];

    if (this.getLevel(currentNode) < currentLevel) {
      return currentNode;
    }
  }
  return null;
}


  /** Select the category so we can insert the new item. */
  addNewItem(node: TodoItemFlatNode) {
    const parentNode = this.flatNodeMap.get(node);
    this.catObj.cat_name = node.item;
    this.parentNodeData = parentNode.children;
    this.database.insertItem(parentNode!, '');
    // parentNode.children.push({ item : '' });
    // parentNode.children.push({ item: '' } as TodoItemNode);
    this.treeControl.expand(node);
  }

  /** Save the node to database */
  saveNode(node: TodoItemFlatNode, itemValue: string) {
    var valueArr = this.parentNodeData.map(function (item) { return item.item });
    var n = valueArr.includes(itemValue);
    if (n) {
      this.snack.open('Category name already exists', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    }
    else {
      this.catObj.subcat_name = itemValue;
      this.userService.setSubCategory(this.catObj).subscribe((data: any) => {
        if (data.success === false) {
          this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
        if (data.success === true) {
          this.getCategories();
          const nestedNode = this.flatNodeMap.get(node);
          // this.database.updateItem(nestedNode!, itemValue);
          this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        }
        if (data[0]) {
          this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      });
    }

    // var isDuplicate = valueArr.some(function (item, idx) {
    //   return valueArr.indexOf(item) != idx
    // });
    // console.log("isDuplicate",isDuplicate);

    //     let hasDup = valueArr.some((val,i)=>{
    //       return valueArr.indexOf(val)!=i
    //     })
    // console.log("hasDup",hasDup);



  }


  saveNewCat(itemValue: string) {
    this.catObj.cat_name = itemValue
    this.userService.setCategory(this.catObj).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.newCat = false
        this.getCategories();
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }


  getCategories() {
    this.userService.getAllCategories().subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.TREE_DATA = data.category;
        this.categoryIdList = data.categoryIdList;
        this.initialize(this.TREE_DATA);

      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }

  deleteItem(node: TodoItemFlatNode, parent:TodoItemFlatNode ) {
    this.catObj.cat_name = parent.item;
    this.catObj.subcat_name = node.item;
    if(confirm("Do you want to delete this category ? ")){
    this.userService.deleteCategory(this.catObj).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.getCategories();
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }
  }
  deleteCategory(node) {
    let id = this.categoryIdList[node.item];
    if(confirm("Do you want to delete this category ? ")){
    this.userService.deleteCategoryById(id).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.getCategories();
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }
  }

}
