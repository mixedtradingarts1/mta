import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-content-details',
  templateUrl: './content-details.component.html',
  styleUrls: ['./content-details.component.css']
})
export class ContentDetailsComponent implements OnInit {

  showReplay = false;
  index: number;
  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private snack: MatSnackBar, ) { }
  id: string;
  comments: any;
  commentObj = {
    id: ''
  }

  reports: any;
  reportCommentObj = {
    comment_id: '',
    report_id: ''
  }

  show = false;
	showReport = false;
	reportObj = {
		text: '',
		content_id: '',
		comment_id: ''
	}

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params.id;
    if (this.id) {
      this.getComments();
      this.getReportComments();

    }
  }

  getComments() {
    this.userService.getAllCommentsById(this.id).subscribe((success) => {

      if (success.status === true) {
        if (success.comments.length > 0) {
          this.comments = success.comments;
        }
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }


  deleteComment(id) {
    this.commentObj.id = id
    this.userService.deleteCommentsById(this.commentObj).subscribe((success) => {

      if (success.success === true) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getComments();
        this.getReportComments();
      }
      if (success.success === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }

  getReportComments() {
    this.userService.getReportCommentsById(this.id).subscribe((success) => {

      if (success.status === true) {
        if (success.comments.length > 0) {
          this.reports = success.comments;
        }
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }

  deleteReportedComment(commentId, reportId) {
    this.reportCommentObj.comment_id = commentId;
    this.reportCommentObj.report_id = reportId;

    this.userService.deleteReportedComment(this.reportCommentObj).subscribe((success) => {

      if (success.success === true) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getReportComments();
        this.getComments();
      }
      if (success.success === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    });
  }


  togglecommnt() {
		if (this.show) {
			this.show = false;
		} else {
			this.show = true;
		}
	}

	togglereport(i: number) {
    this.index = i;
   	if (this.showReport) {
			this.showReport = false;
		} else {
			this.showReport = true;
		}
	}

	forReport(text, id) {
		this.reportObj.content_id = this.id;
		this.reportObj.comment_id = id;
		this.reportObj.text = text;
		this.userService.adminReportComment(this.reportObj).subscribe((success) => {
      if (success.success === true) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getComments();
        this.showReport = false;
			}
			if (success.success === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		});
	}
}
