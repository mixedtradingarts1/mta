import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../guards/auth.guard';
import { LayoutComponent } from './layout/layout.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageCustomerlistComponent } from './manage-customerlist/manage-customerlist.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { MembershipPlanComponent } from './membership-plan/membership-plan.component';
import { ClockSettingComponent } from './clock-setting/clock-setting.component';
import { UploadContentComponent } from './upload-content/upload-content.component';
import { ViewContentComponent } from './view-content/view-content.component';
import { AdminWalletComponent } from './admin-wallet/admin-wallet.component';
import { CategoryComponent } from './category/category.component';
import { DialogComponent } from './dialog/dialog.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { ContentDetailsComponent } from './content-details/content-details.component';
import { JoinwebineruserComponent } from './joinwebineruser/joinwebineruser.component';


// dashboard routing after user successfull login
const routes: Routes = [
  {
    path: '', component: LayoutComponent, canActivate: [AuthGuard], children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'customers', component: ManageCustomerlistComponent },
      { path: 'customerDetails/:id', component: CustomerDetailsComponent },
      { path: 'membership', component: MembershipPlanComponent },
      { path: 'timer', component: ClockSettingComponent },
      { path: 'upload', component: UploadContentComponent },
      { path: 'content', component: ViewContentComponent },
      { path: 'wallet', component: AdminWalletComponent },
      { path: 'category', component: CategoryComponent },
      { path: 'dialog', component: DialogComponent },
      { path: 'subcategory', component: SubCategoryComponent },
      { path: 'contentDetails/:id', component: ContentDetailsComponent },
      { path: 'joinwebineruser', component: JoinwebineruserComponent},
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
