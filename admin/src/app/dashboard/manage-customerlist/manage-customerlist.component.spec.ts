import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCustomerlistComponent } from './manage-customerlist.component';

describe('ManageCustomerlistComponent', () => {
  let component: ManageCustomerlistComponent;
  let fixture: ComponentFixture<ManageCustomerlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCustomerlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCustomerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
