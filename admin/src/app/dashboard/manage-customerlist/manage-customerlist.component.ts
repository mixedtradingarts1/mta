import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MatTableDataSource, MatPaginator, MatSnackBar, PageEvent } from '@angular/material';

@Component({
  selector: 'app-manage-customerlist',
  templateUrl: './manage-customerlist.component.html',
  styleUrls: ['./manage-customerlist.component.css']
})
export class ManageCustomerlistComponent implements OnInit {

  users: any;
  displayedColumns: string[] = ['firstname', 'lastname', 'email', 'phone', 'status','registrationDate', 'action'];
  dataSource = new MatTableDataSource();

  pageIndex = 0;
  pageLimit = [5, 10, 15];
  limitUser = 10;
  totalUserLength = 0;
  search = undefined;
  show = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.gettingAllUsersDetails();
  }


  gettingAllUsersDetails() {
    this.userService.allUsersDetails(this.pageIndex, this.limitUser, this.search).subscribe((users) => {           
      if (users) {
        this.dataSource.data = users.users;
        this.totalUserLength = users.count;
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // searching in Users
  searchUsers(value: any) {
    this.search = value;
    this.gettingAllUsersDetails();
  }
  // chnage limit and previous next button in Users
  getAllUsers(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.limitUser = event.pageSize;
    this.gettingAllUsersDetails();
  }

  updateUserStatus(userId, status) {
    if(confirm("Do you want to change account status?")){
    this.userService.updateUserStatus(userId, status).subscribe((response) => {
      if (response) {
        if (response.success === true) {
          this.gettingAllUsersDetails();
          this.snackBar.open(response.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        }
        else if (response.success === false) {
          this.snackBar.open(response.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }
  }

  DeleteUserStatus(userId, status) {
    if(confirm("Do you want to delete user? ")){
    this.userService.DeleteUserStatus(userId, status).subscribe((response) => {
      if (response) {
        if (response.success === true) {
          this.gettingAllUsersDetails();
          this.snackBar.open(response.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        }
        else if (response.success === false) {
          this.snackBar.open(response.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }
  }


  showDetails() {
    this.show = true;
  }

}
