import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-clock-setting',
  templateUrl: './clock-setting.component.html',
  styleUrls: ['./clock-setting.component.css']
})
export class ClockSettingComponent implements OnInit {

  _csrf: '';
  date: any;
  minDate: any;
  // minTime: any;
  timeObj: any;
  gettime: any;
  getdate: any;

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }

  //timer form
  timerForm = new FormGroup({
    date: new FormControl('', [Validators.required]),
    time: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

  // convenience getter for easy access to form fields
  get fc() { return this.timerForm.controls; }


  ngOnInit() {
    this.csrf();
    this.minDate = new Date();
    // this.minTime = new Date(new Date().setHours(10, 30)),
    this.getClockTimer();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.timerForm.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  setTimer() {
    // stop here if form is invalid
    if (this.timerForm.invalid) {
      return false;
    }
    this.userService.setTimer(this.timerForm.value).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.timerForm.controls.time.reset();
        this.timerForm.controls.date.reset();
        this.getClockTimer();
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }
      , error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
  }


  getClockTimer() {
    this.userService.getClockTimer().subscribe((success) => {
      this.timeObj = success.timer;
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }



}
