# Suzie backend applications

This is the repository for the Suzie backend applications and API.

## Work Environment

The project is being built using ....

## Github flow

There are 2 main branches on git:
- Master: This branch reflects what is in production (App Store).
- Dev: This is the development branch. The development is not done directly in this branch, but on sprint or feature based branches. Once the Pull Request is accepted, it can be merged into this branch.

## Dependencies

This project is currently using **Composer** to manage its dependencies.

To run the project navigate to root and run `php composer.phar install`.
