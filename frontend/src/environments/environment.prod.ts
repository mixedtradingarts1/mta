export const environment = {
  production: true,
  apiEndPoint: 'https://kretoss.com:3016/',
  imageEndPoint: 'https://kretoss.com/project/server/public/',
};
