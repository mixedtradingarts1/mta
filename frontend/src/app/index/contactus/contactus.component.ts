import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  contactusForm:FormGroup;

  constructor(
     private snack: MatSnackBar,
      private userService: UserService,
      private formBuilder:FormBuilder
      ) { }

  ngOnInit() {
    this.contactusForm = this.formBuilder.group({
			'name':['',Validators.required],
			'email':['',[Validators.required,Validators.email]],
			'phone':['',Validators.required],
			'subject':['',Validators.required],
			'description':['',Validators.required]
		});
  }
  onContactusFormSubmit() {
		if(this.contactusForm.valid){
			this.userService.contactUs(this.contactusForm.value).subscribe((success) => {
				if (success.status === true) {
					this.contactusForm.reset();
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				}
				if (success.status === false) {
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
				}
			}, error => {
				this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			});
		}
		else{
			this.contactusForm.markAllAsTouched();
		}
	}
}
