import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { SocketService } from "../../services/socket.service";

@Component({
  selector: 'app-payment-req',
  templateUrl: './payment-req.component.html',
  styleUrls: ['./payment-req.component.css']
})
export class PaymentReqComponent implements OnInit {

  paymentVerify : String;
  userId : String;
  payment_status: boolean;

  @Input()urlstr: SafeResourceUrl;

  constructor(private router: Router,
    private actroute: ActivatedRoute,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
    private sanitizer: DomSanitizer,
    private socketService: SocketService) { }

  ngOnInit() {
    this.paymentVerify = this.actroute.snapshot.paramMap.get('payment');
    console.log("this.paymentVerify", this.paymentVerify);
    if (this.paymentVerify) {
    this.verifyPayment(this.paymentVerify);
    }
   
  }
  // ngOnDestroy() {
  //   this.socketService.unpaymentHandler();
  // }

  verifyPayment(str) {
    this.userService.getPaymentVerified(str).subscribe((result) => {
      if (result.success === true) {
        // console.log("result", result);
        this.userId = result.userid;
        this.urlstr = this.sanitizer.bypassSecurityTrustResourceUrl(result.url);

        if(result.pay_status ==='Paid'){
          this.payment_status = true;
        } else {
          this.payment_status = false;
        }

        this.socketService.onPaymentRedirect(this.userId).subscribe((data) => {
          if (data) {
            this.snack.open('Payment Successful', 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
            // this.router.navigateByUrl('/login');
            if (this.apiService.isLoggedIn()) {
              this.router.navigate(['dashboard']);
            }else{
              this.router.navigateByUrl('/login');
            }
          }
        });
      } 
        if (result.status === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      // this.router.navigateByUrl('/login');
    }, (err) => {
      if (err.error.message == 'Link Expired') {
      }
      this.snack.open(err.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    })
  }

}
