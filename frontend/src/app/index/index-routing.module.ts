import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './index.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { VerificationComponent } from './verification/verification.component';
import { PaymentReqComponent } from './payment-req/payment-req.component';
import { LayoutComponent } from './layout/layout.component';
import { PlanComponent } from './plan/plan.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { AffiliateMarketingComponent } from './affiliate-marketing/affiliate-marketing.component';
import { FeedbackComponent } from './feedback/feedback.component';
import {ContactusComponent} from 'src/app/index/contactus/contactus.component'
import {RequestComponent} from 'src/app/index/request/request.component';
import { BlogComponent } from './blog/blog.component';
import { NewsComponent } from './news/news.component';
import { WebinarComponent} from './webinar/webinar.component';

const routes: Routes = [
  { path: '', component: LayoutComponent ,children: [
    { path: '', component: IndexComponent },   
    { path: 'plan', component: PlanComponent },
    { path: 'register/:id', component: RegistrationComponent },
    { path: 'login', component: LoginComponent },
    { path: 'forgetPassword', component: ForgetPasswordComponent },
    { path: 'verifyemail/:str', component: VerificationComponent },
    { path: 'invoicereq/:payment', component: PaymentReqComponent },
    { path: 'privacypolicy', component: PrivacypolicyComponent },
    { path: 'affiliatemarketing', component: AffiliateMarketingComponent },
    { path: 'feedback', component: FeedbackComponent },
    { path: 'contactus', component: ContactusComponent },
    { path: 'requestform', component: RequestComponent},
    { path: 'newsblog', component: NewsComponent },
    { path: 'newsblog/webinar', component: WebinarComponent }
  ] },
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndexRoutingModule { }
