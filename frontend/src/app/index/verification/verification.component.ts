import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {

  verificationstr: String;

  constructor(private router: Router,
    private actroute: ActivatedRoute,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar) { }

  ngOnInit() {
    this.verificationstr = this.actroute.snapshot.paramMap.get('str');
    if (this.verificationstr) {
      this.verifyemail(this.verificationstr);
    }
  }

  verifyemail(str) {
    this.userService.getEmailVerified(str).subscribe((result) => {
      this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      this.router.navigateByUrl('/');
    }, (err) => {
      if (err.error.message == 'Link Expired') {
      }
      this.snack.open(err.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });

    })
  }

}
