import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {

  planList = [];
  constructor(
    private userService: UserService,
    private snack: MatSnackBar

  ) { }

  ngOnInit() {
    this.getPlans();
  }

  getPlans() {
		this.userService.getPlans().subscribe((success) => {
			if (success.status === true) {
				if (success.plans.length > 0) {
          this.planList = success.plans;
          console.log(this.planList);
				}				
			}			
		});
	}
}
