import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  iPassword: string = null;
  hide = true
  _csrf: '';

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }


  // forgot password form
  resetPasswordForm = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')]),
    confirmPassword: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

   // convenience getter for easy access to form fields
   get fc() { return this.resetPasswordForm.controls; }


  ngOnInit() {
    this.csrf();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.resetPasswordForm.patchValue({ _csrf: this._csrf });

     }, error => {
       this.snack.open(error.message, 'X', { duration: 4000 , panelClass: ['error-snackbar'], horizontalPosition: 'end' });
     });   }

      // store password for confirm password
  StorePassword(event: any): void {
    this.iPassword = event.target.value !== '' ? event.target.value : null;
  }


  // setting custom error for confirm password
  GetInvalidMessage(event: any): void {
    if (event.target.validity.patternMismatch && event.target.id === 'inputConfirmaPassword') {
      event.target.setCustomValidity('Passwords do not match');
    }
  }

  updatePassword(){
       
     }
}
