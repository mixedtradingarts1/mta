import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { environment } from "../../environments/environment";
import { UserService } from "../services/user.service";
import { ApiService } from "../services/api.service";
import { Router } from '@angular/router';
import { Validators, FormGroup,FormBuilder } from '@angular/forms';

declare const $: any;

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.css']
})

export class IndexComponent implements OnInit {

	@ViewChild('videoplayer', { static: false }) videoplayer: ElementRef;
	imgArr: any;
	videoArr: any;
	articleArr: any;
	appurl = '';
	imageEndPoint = '';
	contactForm:FormGroup;


	constructor(private router: Router, private snack: MatSnackBar,
		private apiService: ApiService,
		private userService: UserService,
		private formBuilder:FormBuilder
	) {
		if (environment.production) {
			this.appurl = environment.apiEndPoint;
			this.imageEndPoint = environment.imageEndPoint;
		} else {
			this.appurl = environment.apiEndPoint;
			this.imageEndPoint = environment.imageEndPoint;
		}
		this.contactForm = this.formBuilder.group({
			'name':['',Validators.required],
			'email':['',[Validators.required,Validators.email]],
			'phone':['',Validators.required],
			'subject':['',Validators.required],
			'description':['',Validators.required]
		});
	}

	ngOnInit() {
		if (this.apiService.isLoggedIn()) {
			this.router.navigate(['dashboard']);
		}
		this.getPublicUploads();
		$('.owl-carousel').owlCarousel({
			loop: true,
			margin: 10,
			nav: true,
			autoplay: true,
			navText: ["<img class='bottom-quate' src='assets/images/owl-prev.png' alt='Quate'>", "<img class='bottom-quate' src='assets/images/owl-next.png' alt='Quate'>"],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 1
				}
			}
		});
		
	}

	ngAfterViewInit(){
	
	
	}

	getPublicUploads() {
		this.userService.adminPublicUploads().subscribe((success) => {
			if (success.status === true) {
				this.imgArr = success.image;
				this.videoArr = success.video;
				this.articleArr = success.article;
			}
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}

	playVideo() {	
		this.router.navigate(['login']);		
	}
	onContactFormSubmit() {
		if(this.contactForm.valid){
			this.userService.contactUs(this.contactForm.value).subscribe((success) => {
				if (success.status === true) {
					this.contactForm.reset();
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				}
				if (success.status === false) {
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
				}
			}, error => {
				this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			});
		}
		else{
			this.contactForm.markAllAsTouched();
		}
	}
}
