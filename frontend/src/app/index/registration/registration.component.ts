import { Component, OnInit, ElementRef, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Observable } from 'rxjs/Rx';
import { DialogComponent } from 'src/app/dialog/dialog.component';
import { SocketService } from "../../services/socket.service";

@Component({
	selector: 'app-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
	loading = false;
	_csrf: '';
	hide = true;
	success_msg = false;
	reg_form = true;
	countrylist: any;
	membershipPlans: any;
	timeObj: any;
	formatDate: any;
	formatTime: any;
	joinDateTime: any;
	countDownDate: any;
	timeOut = false;
	newUserFrom = new FormGroup({
		newsletter: new FormControl('', Validators.required),
		PaymentType: new FormControl('', Validators.required),
		ReferralCode: new FormControl(''),
		email: new FormControl('', [Validators.required, Validators.email]),
		password: new FormControl('', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')]),
		fname: new FormControl('', [Validators.required]),
		lname: new FormControl('', [Validators.required]),
		phone: new FormControl(undefined),
		address: new FormControl(''),
		city: new FormControl(''),
		state: new FormControl(''),
		zipcode: new FormControl('', [Validators.pattern('^[0-9]*$')]),
		country: new FormControl(''),
		termconditoion: new FormControl(false),
		_csrf: new FormControl('', [Validators.required]),
		installmentPlan:new FormControl(false)
	});
	dialogRef: MatDialogRef<DialogComponent>;
	dialogConfig = new MatDialogConfig();
	installmentPlan:number;
	timesUp = false;
	noPlans = false;
	selectedPlan = "0";
	selectedPlanDetail = [];
	selectedPlanName="";
	selectedPlanPrice="";
	selectedFullPlanName="";
	constructor(private router: Router,
		private userService: UserService,
		private apiService: ApiService,
		private snack: MatSnackBar, 
		private elementRef: ElementRef,
		public dialog: MatDialog,
		private chatService: SocketService,
		private route: ActivatedRoute,
	) {

	}

	ngOnInit() {
		if (this.apiService.isLoggedIn()) {
			this.router.navigate(['dashboard']);
		}
		this.csrf();
		this.getClockTimer();
		this.getCountry();
		this.getPlans();
		this.reg_form = true;
		var input = document.querySelector("#phone");
		this.route.paramMap.subscribe(params => {
			if (params.has("id")) {
				this.selectedPlan = params.get("id");
			}
		});
		this.chatService.onTimerNotif().subscribe((data) => {
			location.reload(true);
		});
	}
	ngOnDestroy() {
		this.chatService.unregisterHandler()
	}

	// convenience getter for easy access to form fields
	get fc() { return this.newUserFrom.controls; }

	csrf() {
		this.apiService.gtcsrf().subscribe((success) => {
			this._csrf = success._csrf;
			this.newUserFrom.patchValue({ _csrf: this._csrf });
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}


	register() {		
		if (this.newUserFrom.invalid) {
			return false;
		}	
		this.loading = true;		
		if(this.newUserFrom.get("newsletter").value==2){
			this.newUserFrom.patchValue({"installmentPlan":true});
		}
		this.newUserFrom.patchValue({"newsletter":this.selectedPlanName});
		this.userService.registration(this.newUserFrom.value).subscribe((result) => {
			if (result.status === true) {
				this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				this.reg_form = false;
				this.success_msg = true;
			}
			this.loading = false;
			if (result.status === false) {
				this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
			if (result[0]) {
				this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}


	startTimer(display) {		
		var now = new Date().getTime();		
		var timer = (this.countDownDate - now) / 1000;
		var days;
		var hours;
		var minutes;
		var seconds;

		Observable.interval(1000).subscribe(x => {
			days = Math.floor(timer / (60 * 60 * 24));
			hours = Math.floor((timer % (60 * 60 * 24)) / (60 * 60));
			minutes = Math.floor((timer % (60 * 60)) / (60));
			seconds = Math.floor((timer % (60)));
			days = days < 10 ? "0" + days : days;
			hours = hours < 10 ? "0" + hours : hours;
			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;
			display.textContent = days + "D  " + hours + ":" + minutes + ":" + seconds;
			--timer;		
			if (timer < 0) {				
				display.textContent = "00" + "D  " + "00" + ":" + "00" + ":" + "00";
				if (!this.timeOut) {
					this.openDialog();
					this.timeOut = true;
				}				
			}
		});
	}

	getClockTimer() {
		this.userService.getClockTimer().subscribe((success) => {
			this.timeObj = success.timer;
			if (this.timeObj !== null) {
				this.formatDate = this.convertOnlyDate(this.timeObj.date);
				this.formatTime = this.timeConvertor(this.timeObj.time);
				var tt = [this.formatTime, "00"].join(":")
				var timeWithSec = tt.replace(/\s/g, '');
				this.joinDateTime = [this.formatDate, timeWithSec].join(" ");
				if (typeof this.joinDateTime != 'undefined' && this.joinDateTime && this.joinDateTime != '') {
					this.countDownDate = new Date(this.joinDateTime).getTime();
				}
				var callDuration = this.elementRef.nativeElement.querySelector('#time');				
				this.startTimer(callDuration);
				if (success.status === false) {
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
				}
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}

	getCountry() {
		this.userService.getCountries().subscribe((success) => {
			this.countrylist = success;
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}

	getPlans() {
		this.userService.getPlans().subscribe((success) => {
			if (success.status === true) {
				if (success.plans.length > 0) {
					this.membershipPlans = success.plans;
					this.selectedPlanDetail = this.membershipPlans.filter((value) => {
						if (value._id == this.selectedPlan) {							
							this.installmentPlan = value.installmentPlan;
							this.selectedPlanName = value.fullplan;
							this.selectedPlanPrice = value.amount;
							this.selectedFullPlanName = value.fullplan +" "+value.amount+" BTC";						
							this.newUserFrom.patchValue({ "newsletter": '1' });								
							return value.fullplan;
						}
					});

					// if (this.selectedPlanDetail.length > 0 && this.selectedPlanDetail) {
					// 	if (this.selectedPlanDetail[0]) {
					// 		if (this.selectedPlanDetail[0].fullplan) {
								
								
					// 		}
					// 		else {
					// 			this.router.navigate(['/plan']);
					// 		}
					// 	}
					// 	else {
					// 		this.router.navigate(['/plan']);
					// 	}
					// }
					// else {
					// 	this.router.navigate(['/plan']);
					// }				
				}
				else {
					this.openDialogForPlan();
				}
			}
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}

	convertOnlyDate(str) {
		var date = new Date(this.timeObj.date);
		var mySQLDate = date.toDateString();	
		return mySQLDate;
	}

	timeConvertor(time) {
		var PM = time.match('pm') ? true : false
		var hour;
		var min;
		time = time.split(':')
		min = time[1]
		if (PM) {
			hour = 12 + parseInt(time[0], 10)
			min = time[1].replace('pm', '')
		} else {
			hour = time[0]
			min = time[1].replace('am', '')
		}	
		if (time[1].match('pm') && time[0].match('12')) {
			hour = time[0].replace('24', '12')
		} else if (time[1].match('am') && time[0].match('12')) {
			hour = time[0].replace('12', '00')
		}		
		return hour + ':' + min;
	}

	openDialog() {
		this.dialogRef = this.dialog.open(DialogComponent, {
			width: '1000px',
			height: '700px',
			disableClose: true,
			data: this.dialogConfig.data = {
				timesUp: true,
			}
		});
	}
	openDialogForPlan() {
		this.dialogRef = this.dialog.open(DialogComponent, {
			width: '1000px',
			height: '700px',
			disableClose: true,
			data: this.dialogConfig.data = {
				noPlans: true,
			}
		});
	}
}
