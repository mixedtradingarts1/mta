import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-webinar',
  templateUrl: './webinar.component.html',
  styleUrls: ['./webinar.component.css']
})
export class WebinarComponent implements OnInit {

loading = false;
  _csrf: '';
  remember = false;
  hide = true;
  submited : boolean = false;
  form:FormGroup;

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }

  ngOnInit() {
    this.csrf();
    console.log("Demo");
    this.form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    name: new FormControl('', [Validators.required]),
    _csrf: new FormControl(''),
  });
  }

   // convenience getter for easy access to form fields
   get fc() { return this.form.controls; }


   csrf() {
     this.apiService.gtcsrf().subscribe((success) => {
       this._csrf = success._csrf;
       this.form.patchValue({ _csrf: this._csrf });
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000 , panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });   }
 

	submit() {
    this.submited = true;
		if (this.form.invalid) {
			return false;
		}
		this.userService.sendUserJoinMail(this.form.value).subscribe((result) => {
			if (result.status === true) {
        this.submited = false;
        this.form.reset();
        //this.form.markAsPristine();
//this.form.markAsUntouched();
				this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
			}
			if (result.status === false) {
				this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}
}

