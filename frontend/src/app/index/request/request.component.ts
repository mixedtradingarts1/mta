import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  requestForm:FormGroup;

  constructor(
     private snack: MatSnackBar,
      private userService: UserService,
      private formBuilder:FormBuilder
      ) { }

  ngOnInit() {
    this.requestForm = this.formBuilder.group({
			'name':['',Validators.required],
			'email':['',[Validators.required,Validators.email]],
			'phone':['',Validators.required],
			'subject':['Request new feature',Validators.required],
			'description':['',Validators.required]
		});
  }
  onRequestFormSubmit() {
		if(this.requestForm.valid){
			this.userService.sendMail(this.requestForm.value).subscribe((success) => {
				if (success.status === true) {
					this.requestForm.reset();
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				}
				if (success.status === false) {
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
				}
			}, error => {
				this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			});
		}
		else{
			this.requestForm.markAllAsTouched();
		}
	}
}