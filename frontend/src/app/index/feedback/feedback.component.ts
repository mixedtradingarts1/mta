import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  feedbackForm:FormGroup;

  constructor(
     private snack: MatSnackBar,
      private userService: UserService,
      private formBuilder:FormBuilder
      ) { }

  ngOnInit() {
    this.feedbackForm = this.formBuilder.group({
			'name':['',Validators.required],
			'email':['',[Validators.required,Validators.email]],
			'phone':['',Validators.required],
			'subject':['Feedback'],
			'description':['',Validators.required]
		});
  }
  onFeedbackFormSubmit() {
		if(this.feedbackForm.valid){
			this.userService.sendMail(this.feedbackForm.value).subscribe((success) => {
				if (success.status === true) {
					this.feedbackForm.reset();
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				}
				if (success.status === false) {
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
				}
			}, error => {
				this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			});
		}
		else{
			this.feedbackForm.markAllAsTouched();
		}
	}
}