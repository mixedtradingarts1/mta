import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading = false;
  _csrf: '';
  remember = false;
  hide = true;

  // login form
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])

  });

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }

  ngOnInit() {
    this.csrf();
    if (this.apiService.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }
  }

   // convenience getter for easy access to form fields
   get fc() { return this.loginForm.controls; }


   csrf() {
     this.apiService.gtcsrf().subscribe((success) => {
       this._csrf = success._csrf;
       this.loginForm.patchValue({ _csrf: this._csrf });
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000 , panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });   }
 

      login() {
        // stop here if form is invalid
        if (this.loginForm.invalid) {
          return false;
        }
        this.userService.login(this.loginForm.value).subscribe((result) => {
          console.log("result==== login ===>>>>>>>", result);
          if (result.status === true) {
            console.log("result==== login ===>>>>>>>", result);
            
            this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
            this.router.navigateByUrl('/dashboard');
          }
          if (result.status === false) {
            this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
          }
          if (result[0]) {
            this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
          }
        }, error => {
          this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        });
      }

}
