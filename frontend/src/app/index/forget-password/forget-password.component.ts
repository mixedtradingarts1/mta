import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  _csrf: '';
  iPassword: string = null;
  hide = true
  forgetPass_form = true;
  otp_form = false;
  resetPass_form = false;


  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }


  // forgot password form
  forgetpasswordForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    _csrf: new FormControl('', [Validators.required])
  });

  // otp form
  otpForm = new FormGroup({    
    email: new FormControl('', [Validators.required, Validators.email]),
    code: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

  // reset password form
  resetPasswordForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')]),
    confirmPassword: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });


  // convenience getter for easy access to form fields
  get fc() { return this.forgetpasswordForm.controls; }

  get fcOTP() { return this.otpForm.controls; }

  get fcResetPass() { return this.resetPasswordForm.controls; }


  ngOnInit() {
    this.csrf();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.forgetpasswordForm.patchValue({ _csrf: this._csrf });
      this.otpForm.patchValue({ _csrf: this._csrf });
      this.resetPasswordForm.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // send forgot password email
  sendForgotEmail() {
    // stop here if form is invalid
    if (this.forgetpasswordForm.invalid) {
      return false;
    }
    this.userService.forgotPassword(this.forgetpasswordForm.value).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.forgetPass_form = false;
        this.otp_form = true;
        this.resetPass_form = false;
        this.otpForm.patchValue({ email: this.forgetpasswordForm.value.email });

      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  codeVerify() {
    // stop here if form is invalid
    if (this.otpForm.invalid) {
      return false;
    }
    this.userService.otpVerify(this.otpForm.value).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.forgetPass_form = false;
        this.otp_form = false;
        this.resetPass_form = true;
        this.resetPasswordForm.patchValue({ email: this.forgetpasswordForm.value.email });
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // store password for confirm password
  StorePassword(event: any): void {
    this.iPassword = event.target.value !== '' ? event.target.value : null;
  }

  // setting custom error for confirm password
  GetInvalidMessage(event: any): void {
    if (event.target.validity.patternMismatch && event.target.id === 'inputConfirmaPassword') {
      event.target.setCustomValidity('Passwords do not match');
    }
  }

  updatePassword() {
      // stop here if form is invalid
      if (this.resetPasswordForm.invalid) {
        return false;
      }
      this.userService.resetPassword(this.resetPasswordForm.value).subscribe((data: any) => {
        if (data.success === false) {
          this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
        if (data.success === true) {
          this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
          this.router.navigate(['login']);
        }
        if (data[0]) {
          this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
  }
}
