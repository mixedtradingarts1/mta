import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService} from 'src/app/services/api.service' 
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  constructor(private router:Router,private apiService:ApiService) { }

  ngOnInit() {
  }

  redirect(){
  	if (this.apiService.isLoggedIn()) {
  	this.router.navigate(['/news/webinar']);
  }else{
  	this.router.navigate(['/newsblog/webinar']);

  }
  }

}
