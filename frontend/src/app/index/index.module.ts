import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IndexRoutingModule } from './index-routing.module';
import { IndexComponent } from './index.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CustomMaterialModule } from '../material.module';
import { VerificationComponent } from './verification/verification.component';
import { NgxMatIntlTelInputModule } from './../../../node_modules/ngx-mat-intl-tel-input';
import { BsDropdownModule } from 'ngx-bootstrap';
import { OtpFormComponent } from './otp-form/otp-form.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PaymentReqComponent } from './payment-req/payment-req.component';
import { LayoutComponent } from './layout/layout.component';
import { MatVideoModule } from 'mat-video';
import { PlanComponent } from './plan/plan.component';
import { SharedModule } from '../shared/shared.module';






@NgModule({
  declarations: [IndexComponent,
    RegistrationComponent,
    LoginComponent,
    ForgetPasswordComponent,
    VerificationComponent,
    OtpFormComponent,
    ResetPasswordComponent,
    PaymentReqComponent,
    LayoutComponent,
    PlanComponent        
  ],
  imports: [
    CommonModule,
    IndexRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CustomMaterialModule,
    NgxMatIntlTelInputModule,
    MatVideoModule,
    BsDropdownModule.forRoot(),
    SharedModule
  ],
  //exports:[SafePipe]
})
export class IndexModule { }
