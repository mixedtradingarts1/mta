import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-otp-form',
  templateUrl: './otp-form.component.html',
  styleUrls: ['./otp-form.component.css']
})
export class OtpFormComponent implements OnInit {
  _csrf: '';

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
  ) { }


  // forgot password form
  otpForm = new FormGroup({
    code: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

   // convenience getter for easy access to form fields
   get fc() { return this.otpForm.controls; }


  ngOnInit() {
    this.csrf();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.otpForm.patchValue({ _csrf: this._csrf });

     }, error => {
       this.snack.open(error.message, 'X', { duration: 4000 , panelClass: ['error-snackbar'], horizontalPosition: 'end' });
     });   }

     codeVerify(){
       
     }

}
