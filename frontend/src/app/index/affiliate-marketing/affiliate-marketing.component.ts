import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material';
import { ProfileService } from 'src/app/services/profile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-affiliate-marketing',
  templateUrl: './affiliate-marketing.component.html',
  styleUrls: ['./affiliate-marketing.component.css']
})
export class AffiliateMarketingComponent implements OnInit {
	affiliateMarketingForm:FormGroup;
  details = {
    api_key_status:null
  };
  constructor(
     private snack: MatSnackBar,
      private userService: UserService,
      private formBuilder:FormBuilder,
      private profileService:ProfileService,
      private router:Router
      ) { }

  ngOnInit() {
    this.affiliateMarketingForm = this.formBuilder.group({
			'name':['',Validators.required],
			'email':['',[Validators.required,Validators.email]],
			'phone':['',Validators.required],
			'subject':['Affiliate and marketing'],
			'description':['',Validators.required]
    });
    
  }
  onAffiliateMarketingFormSubmit() {
		if(this.affiliateMarketingForm.valid){
			this.userService.sendMail(this.affiliateMarketingForm.value).subscribe((success) => {
				if (success.status === true) {
					this.affiliateMarketingForm.reset();
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				}
				if (success.status === false) {
					this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
				}
			}, error => {
				this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			});
		}
		else{
			this.affiliateMarketingForm.markAllAsTouched();
		}
  }
  
  
}
