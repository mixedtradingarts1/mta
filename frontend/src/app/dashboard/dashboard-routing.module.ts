import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guard/auth.guard';

import { DashboardComponent } from './dashboard/dashboard.component';
import { TradeComponent } from './trade/trade.component';
import { ProfileComponent } from './profile/profile.component';
import { LayoutComponent } from './layout/layout.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { LoginLogsComponent } from './login-logs/login-logs.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { UserTradeComponent } from './user-trade/user-trade.component';
import {PrivacypolicyComponent} from 'src/app/index/privacypolicy/privacypolicy.component' 
import { AffiliateMarketingComponent } from 'src/app/index/affiliate-marketing/affiliate-marketing.component';
import { FeedbackComponent } from 'src/app/index/feedback/feedback.component';
import {ContactusComponent} from 'src/app/index/contactus/contactus.component'
import {RequestComponent} from 'src/app/index/request/request.component';
import { BlogComponent } from 'src/app/index/blog/blog.component';
import { NewsComponent } from '../index/news/news.component';
import { ContentComponent } from './content/content.component';
import { WebinarComponent } from 'src/app/index/webinar/webinar.component';
const routes: Routes = [
  {
    path: '', component: LayoutComponent,canActivate:[AuthGuard],children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'trade', component: TradeComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'account', component: UseraccountComponent },
      { path: 'changepassword', component: ChangePasswordComponent },
      { path: 'profile/:edit', component: ProfileComponent },
      { path: 'loginlogs', component: LoginLogsComponent },
      { path: 'paymenthistory', component: PaymentHistoryComponent },
      { path: 'usertrade', component: UserTradeComponent },
      { path: 'privacy-policy', component: PrivacypolicyComponent },
      { path: 'affiliate-marketing', component: AffiliateMarketingComponent },
      { path: 'userfeedback', component: FeedbackComponent },
      { path: 'usercontactus', component: ContactusComponent },
      { path: 'request-form', component: RequestComponent},
      { path: 'news', component: NewsComponent},
      { path: 'news/webinar', component: WebinarComponent},
      { path: 'content/:id', component: ContentComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
