import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { ProfileService } from "../../services/profile.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.css']
})
export class PaymentHistoryComponent implements OnInit {
  _csrf: '';

  details = {
    fname: '',
    lname: '',
    address: '',
    city: '',
    state: '',
    zipcode: null,
    country: '',
    avatar: '',
    email: '',
    phone: '',
    email_verified: null,
    phone_verified: null,
    newsletter: '',
  };

  payData: any;
  showform = false;
  membershipPlans: any;

  renewPlanFrom = new FormGroup({
    plan: new FormControl('', Validators.required),
    _csrf: new FormControl('', [Validators.required])
  });


  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private profileService: ProfileService,
    private snack: MatSnackBar,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.csrf();
    this.getProfile();
    this.getPaymentHistory();
  }
// convenience getter for easy access to form fields
get fc() { return this.renewPlanFrom.controls; }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.renewPlanFrom.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getProfile() {
    this.profileService.getUserProfile().subscribe((result) => {
      this.details = result.user;
      if (result.success === true) {
        this.csrf();
      }
      if (result.success === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getPaymentHistory() {
    this.userService.getpaymentHistory().subscribe((result) => {
      if (result.success === true) {
        this.payData = result.payment;
        this.csrf();
      }
      if (result.success === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getPlans() {
    this.userService.getPlans().subscribe((success) => {
      if (success.status === true) {

        if (success.plans.length > 0) {
          this.membershipPlans = success.plans;          
        }
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  openForm(){
    this.showform = true;
    this.getPlans();
  }

  renewPlan(){    
    this.userService.planrenewalReq(this.renewPlanFrom.value).subscribe((success) => {
      if (success.status === true) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }
  
}
