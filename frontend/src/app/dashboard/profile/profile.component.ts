import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { ProfileService } from "../../services/profile.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  _csrf: '';
  showform: String = '';
  details = {
    fname: '',
    lname: '',
    address: '',
    city: '',
    state: '',
    zipcode: null,
    country: '',
    avatar: '',
    email: '',
    phone: '',
    email_verified: null,
    phone_verified: null
  };
  profileObj = {
    fname: '',
    lname: '',
    address: '',
    city: '',
    state: '',
    zipcode: null,
    country: '',
    avatar: '',
    _csrf: ''
  }

  countrylist: any;
  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private profileService: ProfileService,
    private snack: MatSnackBar,
    private route: ActivatedRoute,
  ) { }

  // edit profile form
  ProfileEditForm = new FormGroup({
    fname: new FormControl('', [Validators.required]),
    lname: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    state: new FormControl('', [Validators.required]),
    zipcode: new FormControl('', [Validators.required]),
    country: new FormControl('', [Validators.required]),
    avatar: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

  // convenience getter for easy access to form fields
  get fc() { return this.ProfileEditForm.controls; }

  ngOnInit() {
    this.csrf();
    // this.showform = 'view';
    this.getProfile();
    this.getCountry();
    var queryparam = Object.keys(this.route.snapshot.params);
    if (queryparam.length === 0) {
      this.showform = 'view';
    } else {
      if (queryparam[0] === 'edit') {
        this.showform = 'edit';
      }
    }
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.profileObj._csrf = success._csrf;
      this.ProfileEditForm.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getProfile() {
    this.profileService.getUserProfile().subscribe((result) => {
      // console.log("Profile get result", result);
      this.details = result.user;
      this.profileObj = result.user;
      if (result.success === true) {
        this.csrf();
      }
      if (result.success === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getCountry() {
    this.userService.getCountries().subscribe((success) => {
      this.countrylist = success;
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  selectAvatar(img) {
    this.profileObj.avatar = img;
  }

  updateProfile() {
    // stop here if form is invalid
    // if (this.ProfileEditForm.invalid) {
    //   return false;
    // }
    this.profileService.updateProfile(this.profileObj).subscribe((result) => {
      if (result.success === true) {
        ;
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.showform = 'view';
      }
      if (result.success === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }
}
