import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { ProfileService } from "../../services/profile.service";
import { OwlOptions } from "ngx-owl-carousel-o";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';


export interface trxElement {
  Plan: string;
  TxId: string;
  Amount: number;
  Date: string;
}

const ELEMENT_DATA: trxElement[] = [
  {TxId: 'eeffgf4545454510', Plan: 'Yearly', Amount: 1.0079, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: '4fff5454fje54510', Plan: 'Monthly', Amount: 4.0026, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: 'aaaddd4545454510', Plan: 'Monthly', Amount: 6.941, Date: 'LOct 11, 2019, 10:31:31 AMi'},
  {TxId: 'ioeppr4545454510', Plan: 'Beryllium', Amount: 9.0122, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: '4fsdfdd545454510', Plan: 'Yearly', Amount: 10.811, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: 'f45454bbbbb54510', Plan: 'Monthly', Amount: 12.0107, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: 'dklgrr4545454510', Plan: 'Yearly', Amount: 14.0067, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: 'topjkl4545454510', Plan: 'Yearly', Amount: 15.9994, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: 'zxsdfg4545454510', Plan: 'Monthly', Amount: 18.9984, Date: 'Oct 11, 2019, 10:31:31 AM'},
  {TxId: 'qwerrt4545454510', Plan: 'Monthly', Amount: 20.1797, Date: 'Oct 11, 2019, 10:31:31 AM'},
];




@Component({
  selector: 'app-useraccount',
  templateUrl: './useraccount.component.html',
  styleUrls: ['./useraccount.component.css']
})
export class UseraccountComponent implements OnInit {
  _csrf: '';
  showform: String = '';
  details = {
    fname: '',
    lname: '',
    address: '',
    city: '',
    state: '',
    zipcode: null,
    country: '',
    avatar: '',
    email:'',
    phone: '',
    email_verified: null,
    phone_verified: null,
    api_key_status: null,
    api_key_id: '',
    api_key_secret: '',
  };
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
    nav: true
  }
  logsdata: any;

  pageIndex = 0;
  pageLimit = [5, 10, 15];
  limitUser = 10;

  transactionSource = new MatTableDataSource();
  trasactionColumns: string[] = ['Plan', 'TxId', 'Amount', 'Date'];

  subsData = {
    newsletter: '',
    btcamount: null,
    expiry_date: null
  }
  membershipPlans: any;
  color = 'green';

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private profileService: ProfileService,
    private snack: MatSnackBar,
  ) { }

   // membership plan form
   keyForm = new FormGroup({
    key: new FormControl('', [Validators.required]),
    id: new FormControl('', [Validators.required]),
    _csrf: new FormControl('', [Validators.required])
  });

  // convenience getter for easy access to form fields
  get fc() { return this.keyForm.controls; }

 ngOnInit() {
    this.csrf();
    this.getProfile();
    this.transactionSource.data = ELEMENT_DATA;
    this.getloginlogs();
    this.getUserCurrentSubscription();
    this.getPlans();
  }

  csrf() {
    this.apiService.gtcsrf().subscribe((success) => {
      this._csrf = success._csrf;
      this.keyForm.patchValue({ _csrf: this._csrf });
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getProfile() {
    this.profileService.getUserProfile().subscribe((result) => {
      this.details = result.user;
      if (result.success === true) {
        this.csrf();
      }
      if (result.success === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  getloginlogs() {
    this.userService.getloginlogs(this.pageIndex, this.limitUser).subscribe((data) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        if (data) {
          this.logsdata = data.logs[0];
        }
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getUserCurrentSubscription() {
    this.userService.getUserCurrentSubscription().subscribe((result) => {
      if (result.success === true) {
        this.subsData = result.subs;        
      }
      if (result.success === false) {
        this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getPlans() {
    this.userService.getPlans().subscribe((success) => {
      if (success.status === true) {

        if (success.plans.length > 0) {
          this.membershipPlans = success.plans;     
        }
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  apiKeySubmit(){
    
    
   // stop here if form is invalid
    if (this.keyForm.invalid) {
      return false;
    }
    this.userService.addBitmexAccess(this.keyForm.value).subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.keyForm.controls.key.reset();
        this.keyForm.controls.id.reset();
        this.getProfile();
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  removeAccess(){
    if (confirm("Do you want to delete your credentials? ")) {
    this.userService.removeBitmexAccess().subscribe((data: any) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
        this.getProfile();
      }
      if (data[0]) {
        this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });

  }
  }

}
