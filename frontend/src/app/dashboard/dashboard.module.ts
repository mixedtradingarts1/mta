import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from '../material.module';
import { TradeComponent } from './trade/trade.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LayoutComponent } from './layout/layout.component';
import { ProfileComponent } from './profile/profile.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { LoginLogsComponent } from './login-logs/login-logs.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { UserTradeComponent } from './user-trade/user-trade.component';
import { SharedModule } from '../shared/shared.module';
import { ContentComponent } from './content/content.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';


@NgModule({
  declarations: [DashboardComponent, TradeComponent, LayoutComponent, ProfileComponent, UseraccountComponent, ChangePasswordComponent, LoginLogsComponent, PaymentHistoryComponent, UserTradeComponent,ContentComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    CustomMaterialModule,
    MatTooltipModule,
    CarouselModule,
    SharedModule,
    NgxAudioPlayerModule
  ]
})
export class DashboardModule { }
