import { Component, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MatTableDataSource, MatPaginator, MatSnackBar, PageEvent, MatTabGroup, MatSort, Sort, MatSlideToggleChange } from '@angular/material';
import { SocketService } from "../../services/socket.service";
import { Router } from '@angular/router';
import { ProfileService } from "../../services/profile.service";

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {

  users: any;
  marginData: any;
  marginObj = {};
  displayedColumns: string[] = ['Symbol', 'Qty', 'OrderPrice', 'Filled', 'StopPrice', 'FillPrice', 'Type', 'Status', 'OrderId', 'Time'];
  PositionColumns: string[] = ['Symbol', 'size', 'value', 'enrtyPrice', 'markPrice', 'liqPrice', 'margin', 'UnrealisedPNL', 'RealisedPNL', 'ClosePosition'];
  closePositionColumns: string[] = ['Symbol', 'RealisedPNL'];
  fillsColumns: string[] = ['Symbol', 'Qty', 'ExecQty', 'Remaining', 'ExecPrice', 'OrderPrice', 'Value', 'Type', 'OrderId', 'Time'];
  stopsColumns: string[] = ['Symbol', 'Qty', 'OrderPrice', 'Filled', 'StopPrice', 'TriggeringPrice', 'FillPrice', 'Type', 'Status', 'Time', 'OrderId'];
  oredrsColumns: string[] = ['Symbol', 'Qty', 'OrderPrice', 'Filled', 'Remaining', 'FillPrice', 'Type', 'Status', 'OrderId', 'Time'];


  dataSource = new MatTableDataSource();
  PositionSource = new MatTableDataSource();
  closePositionSource = new MatTableDataSource();
  stopsSource = new MatTableDataSource();
  fillsSource = new MatTableDataSource();
  orderSource = new MatTableDataSource();


  fetchdata: any;
  pageIndex = 0;
  pageLimit = [5, 10, 15];
  limitOrder = 10;
  totalOrderLength = 0;
  search = undefined;
  show = false;
  triggerPrice: any;

  sortedData;
  filledsortedData;
  instrumentData: any;
  oldinstrumentData: any;

  greenIns = false;
  redIns = false;
  symbolArr = [];
  symbolArr1 = [];

  symbolArrPos = [];
  symbolArrPos1 = [];
  useDefault: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  details = {
    api_key_status:null
  };
  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    private socketService: SocketService,
    private router:Router,
    private profileService: ProfileService,
  ) { }

  ngOnInit() {
    this.getAdminBitmexAccount();
    this.getProfile();
    // this.getOrders();
    // // this.getTrade();
    // this.getFilledOrders();
    // this.getMargin();
    // this.getHTTPPositions();
    // this.getActiveOrders();
    // this.getInstrument();
  }

  textChange(event) {
    //console.log("hello", event);

  }

  getAdminBitmexAccount() {
    this.userService.getAdminBitmexAccount().subscribe((success) => {
      //console.log("getuserdata===>>>, userid", success);
      if (success.success) {
       // console.log("Admin Data Connected..");
        this.getOrders();
        // this.getTrade();
        this.getFilledOrders();
        this.getMargin();
        this.getHTTPPositions();
        this.getActiveOrders();
        this.getInstrument();
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getProfile() {
    this.profileService.getUserProfile().subscribe((result) => {
      // console.log("Profile get result", result);
      if (result.success === true) {
        this.details = result.user;
       if(result.user.newsletter=='Free'){
        this.router.navigate(['/dashboard']);
       }
        this.details.api_key_status = result.user.api_key_status;
      }
      if (result.success === false) {
        this.snackBar.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (result[0]) {
        this.snackBar.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  getOrders() {
    this.userService.getOrders(this.pageIndex, this.limitOrder, this.search).subscribe((success) => {
      if (success) {
        this.dataSource.data = success.data;
        console.log(this.dataSource.data);
        // this.dataSource.sort = this.sort;
        this.sortedData = this.dataSource.data.slice();
        // let obj = this.dataSource.data.find(o => o['ordStatus'] === 'Filled');
        this.totalOrderLength = success.count;
        this.getOrdersWS();
        this.getStopOrders(this.dataSource.data);
        // this.getFilledOrders(this.dataSource.data);
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  orderHistorysortData(sort: Sort) {
    const data = this.sortedData;
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }
    this.dataSource.data = data.sort((a: any, b: any) => {
      // console.log(a.cumQty, b.cumQty);
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Symbol': return this.compare(a.symbol, b.symbol, isAsc);
        case 'Qty': return this.compare(a.orderQty, b.orderQty, isAsc);
        case 'OrderPrice': return this.compare(a.price, b.price, isAsc);
        case 'Filled': return this.compare(a.cumQty, b.cumQty, isAsc);
        case 'StopPrice': return this.compare(a.stopPx, b.stopPx, isAsc);
        case 'FillPrice': return this.compare(a.avgPx, b.avgPx, isAsc);
        case 'Type': return this.compare(a.ordType, b.ordType, isAsc);
        case 'Status': return this.compare(a.ordStatus, b.ordStatus, isAsc);
        case 'OrderId': return this.compare(a.orderID, b.orderID, isAsc);
        case 'Time': return this.compare(a.transactTime, b.transactTime, isAsc);
        default: return 0;
      }
    });
  }

  fillsortData(sort: Sort) {
    const data = this.filledsortedData;
    if (!sort.active || sort.direction === '') {
      this.fillsSource.data = data;
      return;
    }
    this.fillsSource.data = data.sort((a: any, b: any) => {
      // console.log(a.cumQty, b.cumQty);
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Symbol': return this.compare(a.symbol, b.symbol, isAsc);
        case 'Qty': return this.compare(a.orderQty, b.orderQty, isAsc);
        case 'ExecQty': return this.compare(a.cumQty, b.cumQty, isAsc);
        case 'Remaining': return this.compare(a.leavesQty, b.leavesQty, isAsc);
        case 'ExecPrice': return this.compare(a.lastPx, b.lastPx, isAsc);
        case 'OrderPrice': return this.compare(a.price, b.price, isAsc);
        case 'Value': return this.compare(a.execCost, b.execCost, isAsc);
        case 'Type': return this.compare(a.ordType, b.ordType, isAsc);
        case 'OrderId': return this.compare(a.orderID, b.orderID, isAsc);
        case 'Time': return this.compare(a.transactTime, b.transactTime, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  getOrdersWS() {
    this.socketService.onOrdersData().subscribe((data) => {
      if (data) {
        // this.marginData = data.data;
        this.getOrders();
        this.getFilledOrders();
        this.getMargin();
        this.getHTTPPositions();
        this.getActiveOrders();
      }
    });
  }


  getStopOrders(value: any) {
    var stopData = [];
    value.forEach((item) => {
      if (Object.keys(item).length > 0) {
        if (item.ordType === 'Stop' && item.ordStatus === 'New') {
          stopData.push(item);
        }
        this.stopsSource.data = stopData;        
      }
    })
  }

  getFilledOrders() {
    this.userService.getFillOrders().subscribe((success) => {
      if (success) {
        console.log(success.data);
        this.fillsSource.data = success.data;
        this.filledsortedData = this.fillsSource.data.slice();

      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getActiveOrders() {
    this.userService.getActiveOrders().subscribe((success) => {
      if (success) {
        var activeOrdData = [];
        success.data.forEach((item) => {
          if (Object.keys(item).length > 0) {
            if (item.ordType === 'Limit') {
              activeOrdData.push(item);
            }
            this.orderSource.data = activeOrdData;
            console.log(this.orderSource.data);
          }
        })
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // getFilledOrders(value: any) {
  //   var fillData = [];
  //   value.forEach((item) => {
  //     if (Object.keys(item).length > 0) {
  //       if (item.ordStatus === 'Filled') {
  //         fillData.push(item);
  //       }
  //       this.fillsSource.data = fillData;
  //     }
  //   })
  // }

  // searching in Users
  searchUsers(value: any) {
    this.search = value;
    this.getOrders();
  }

  // chnage limit and previous next button in Users
  getAllOrders(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.limitOrder = event.pageSize;
    this.getOrders();
  }

  getMargin() {
    this.socketService.onMarginData().subscribe((data) => {
      if (data) {
        this.marginData = data.data;
        // console.log(" this.marginData Admin",  this.marginData);
        
      }
    });
  }

  getTrade() {
    this.socketService.onTradeData().subscribe((data) => {
      if (data) {
        data.data.forEach((item) => {
          if (Object.keys(item).length > 0) {
          }
        })
      }
    });
  }


  getHTTPPositions() {
    this.userService.getPositions().subscribe((success) => {
      if (success) {
        this.fetchdata = success.data;
        this.PositionSource.data = success.data;        
        this.getPositionWS(this.PositionSource.data);
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  getPositionWS(value: any) {
    this.socketService.onPositionData().subscribe((data) => {
      if (data) {
        if (typeof data.data !== 'undefined' && data.data.length > 0) {
          data.data.forEach((lists) => {
            var oldFilterData = value.filter(obj => obj.symbol === lists.symbol);

            if (Object.keys(lists).length > 0) {
              var OldData = this.PositionSource.data;
              var closedData = this.PositionSource.data;
              var objIndex = OldData.findIndex(obj => obj['symbol'] === lists.symbol);
              var newfilterData = data.data.filter(obj => obj.symbol === lists.symbol);

              if (typeof objIndex !== 'undefined' && objIndex > -1) {
                OldData.splice(objIndex, 1, lists);
              }
              this.PositionSource.data = OldData;
              var cpObjIndex = closedData.findIndex(obj => obj['avgEntryPrice'] === null);
              if (typeof cpObjIndex !== 'undefined' && cpObjIndex > -1) {

                if (oldFilterData[0].markPrice > newfilterData[0].markPrice) {
                  this.redIns = true;
                  this.symbolArr[oldFilterData[0].symbol] = oldFilterData[0].symbol;                  
                  setTimeout(() => {
                    this.symbolArr = [];
                    this.redIns = false;
                  }, 500);

                }
                else if (oldFilterData[0].markPrice < newfilterData[0].markPrice) {
                  this.greenIns = true;
                  this.symbolArr1[oldFilterData[0].symbol] = oldFilterData[0].symbol;
                  setTimeout(() => {
                    this.symbolArr1 = [];
                    this.greenIns = false;
                  }, 500);
                }
                // this.closePositionSource.data.push(lists);
                this.closePositionSource.data.splice(cpObjIndex, 1, lists);
                this.PositionSource.data.splice(cpObjIndex, 1);
                // this.closePositionSource.data[cpObjIndex] = data.data;
              }
              this.closePositionSource.data = closedData;
            }
          })
        }
      }
    });
  }



  getInstrument() {
    this.userService.getInstrument().subscribe((success) => {
      if (success) {
        this.instrumentData = success.data;
        this.oldinstrumentData = success.data;
        this.getInstrumentWS();
      }
    }, error => {
      this.snackBar.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }


  getInstrumentWS() {
    this.socketService.onInstrumentData().subscribe((data) => {
      if (data) {
        if (typeof data.data !== 'undefined' && data.data.length > 0) {
          data.data.forEach((lists) => {
            var oldFilterData = this.oldinstrumentData.filter(obj => obj.symbol === lists.symbol);
            if (Object.keys(lists).length > 0) {
              var OldData = this.instrumentData;
              var objIndex = OldData.findIndex(obj => obj['symbol'] === lists.symbol);
              var filterData = data.data.filter(obj => obj.symbol === lists.symbol);
              if (typeof objIndex !== 'undefined' && objIndex > -1) {
                // console.log("oldFilterData", oldFilterData[0].symbol, oldFilterData[0].lastPrice, filterData[0].lastPrice, oldFilterData[0].lastPrice > filterData[0].lastPrice, oldFilterData[0].lastPrice < filterData[0].lastPrice);
                
                if (oldFilterData[0].lastPrice > filterData[0].lastPrice) {
                  this.redIns = true;
                  this.symbolArr[oldFilterData[0].symbol] = oldFilterData[0].symbol;
                  
                  setTimeout(() => {
                    this.symbolArr = [];
                    this.redIns = false;
                  }, 500);

                }
                else if (oldFilterData[0].lastPrice < filterData[0].lastPrice) {
                  this.greenIns = true;
                  this.symbolArr1[oldFilterData[0].symbol] = oldFilterData[0].symbol;
                  setTimeout(() => {
                    this.symbolArr1 = [];
                    this.greenIns = false;
                  }, 500);
                }

                if (oldFilterData[0].lastChangePcnt > filterData[0].lastChangePcnt) {
                  this.redIns = true;
                  this.symbolArr[oldFilterData[0].symbol] = oldFilterData[0].symbol;                  
                  setTimeout(() => {
                    this.symbolArr = [];
                    this.redIns = false;
                  }, 500);

                }
                else if (oldFilterData[0].lastChangePcnt < filterData[0].lastChangePcnt) {
                  this.greenIns = true;
                  this.symbolArr1[oldFilterData[0].symbol] = oldFilterData[0].symbol;
                  setTimeout(() => {
                    this.symbolArr1 = [];
                    this.greenIns = false;
                  }, 500);
                }


                OldData.splice(objIndex, 1, lists);
              }
              this.instrumentData = OldData;
            }
          })
        }
      }
    });

  }


  toggle(event: MatSlideToggleChange) {
    console.log('Toggle fired');
    this.useDefault = event.checked;
    console.log("this.useDefault ",this.useDefault );
    if (this.useDefault) {
      this.router.navigateByUrl('/usertrade');
    }
  }

  changeOrderData(){
    alert("hello")
  }
}
