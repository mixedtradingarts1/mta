import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { ProfileService } from "../../services/profile.service";
import { SocketService } from "../../services/socket.service";
import { DialogComponent } from 'src/app/dialog/dialog.component';
import { environment } from "../../../environments/environment";
import { DomSanitizer } from '@angular/platform-browser';
import { zoomInAnimate } from 'src/app/app.animation';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';

/**
 * Node for to-do item
 */
export class TodoItemNode {
	children: TodoItemNode[];
	item: string;
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
	item: string;
	level: number;
	expandable: boolean;
}

@Component({
	selector: 'app-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
	activeNode: any;
	_csrf: '';
	userid: any;
	accountType: string;
	details = {
		fname: '',
		lname: '',
		address: '',
		city: '',
		state: '',
		zipcode: null,
		country: '',
		avatar: '',
		email: '',
		phone: '',
		email_verified: null,
		phone_verified: null
	};
	membershipPlans: any;
	dialogRef: MatDialogRef<DialogComponent>;
	dialogConfig = new MatDialogConfig();
	categoryIdList = [];
	TREE_DATA: any;
	treedatasource: any;
	newCat = false;
	catObj = {
		cat_name: '',
		subcat_name: ''
	}
	selectedDataArr: any = [];
	parentNodeData: any;
	/** Map from flat node to nested node. This helps us finding the nested node to be modified */
	flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();
	/** Map from nested node to flattened node. This helps us to keep the same object for selection */
	nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();
	/** A selected parent node to be inserted */
	selectedParent: TodoItemFlatNode | null = null;
	/** The new item's name */
	newItemName = '';
	treeControl: FlatTreeControl<TodoItemFlatNode>;
	treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
	dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;
	/** The selection for checklist */
	checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);
	constructor(private router: Router,
		private userService: UserService,
		public apiService: ApiService,
		private snack: MatSnackBar,
		private profileService: ProfileService,
		private socketService: SocketService,
		public dialog: MatDialog,
	) {
		this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
			this.isExpandable, this.getChildren);
		this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
		this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
	}


	getLevel = (node: TodoItemFlatNode) => node.level;

	isExpandable = (node: TodoItemFlatNode) => node.expandable;

	getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

	hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

	hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

	/**
	 * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
	 */
	transformer = (node: TodoItemNode, level: number) => {
		const existingNode = this.nestedNodeMap.get(node);
		const flatNode = existingNode && existingNode.item === node.item
			? existingNode
			: new TodoItemFlatNode();
		flatNode.item = node.item;
		flatNode.level = level;
		flatNode.expandable = !!node.children;
		this.flatNodeMap.set(flatNode, node);
		this.nestedNodeMap.set(node, flatNode);
		return flatNode;
	}

	ngOnInit() {
		this.getProfile();
		// this.getPlans();
		this.getCategories();
	}

	logout() {
		this.apiService.logout();
		this.snack.open(
			'You are logged out successfully!', 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
		this.router.navigate(['']);
	}


	csrf() {
		this.apiService.gtcsrf().subscribe((success) => {
			this._csrf = success._csrf;
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}

	getProfile() {
		this.profileService.getUserProfile().subscribe((result) => {
			this.details = result.user;
			if (result.success === true) {
				this.accountType = result.user.newsletter;
				this.userid = result.user._id;
				var date1 = new Date();
				var date2 = new Date(result.user.expiry_date);
				var Difference_In_Time = date2.getTime() - date1.getTime();
				var Difference_In_Days = Math.round(Difference_In_Time / (1000 * 3600 * 24));
				if (Difference_In_Days < 6 && Difference_In_Days >= 0) {
					this.openAlertForPayment(Difference_In_Days, result.user.expiry_date);
				}
				else if (Difference_In_Days < 1) {
					this.userService.expiryCheck().subscribe((success) => {
						if (success.status === true) {
							this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
							location.reload(true);
						}
					})
				}
				if (result.user.payment_verified === 0 && result.user.newsletter != "Free") {
					this.getPlans();
				}
				this.updatedProfile();
				this.csrf();
			}
			if (result.success === false) {
				this.snack.open(result.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
			if (result[0]) {
				this.snack.open(result[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}
	onActivate(event) {
		window.scroll(0, 0);

	}

	updatedProfile() {
		this.socketService.onUserProfileUpdate(this.userid).subscribe((data) => {
			if (data) {
				this.getProfile();
			}
		});
	}

	getPlans() {
		this.userService.getPlans().subscribe((success) => {
			if (success.status === true) {
				if (success.plans.length > 0) {
					this.membershipPlans = success.plans;
					this.openDialogPayment(this.details, this.membershipPlans);
				}
			}
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		}, error => {
			this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
		});
	}

	initialize(treedata) {
		this.treedatasource = this.buildFileTree(treedata, 0);
		this.dataSource.data = this.treedatasource;
	}

	buildFileTree(obj: object, level: number): TodoItemNode[] {
		return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
			const value = obj[key];
			const node = new TodoItemNode();
			node.item = key;
			if (value != null) {
				if (typeof value === 'object') {
					node.children = this.buildFileTree(value, level + 1);
				} else {
					node.item = value;
				}
			}
			return accumulator.concat(node);
		}, []);
	}

	/* Get the parent node of a node */
	getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
		const currentLevel = this.getLevel(node);
		if (currentLevel < 1) {
			return null;
		}
		const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
		for (let i = startIndex; i >= 0; i--) {
			const currentNode = this.treeControl.dataNodes[i];
			if (this.getLevel(currentNode) < currentLevel) {
				return currentNode;
			}
		}
		return null;
	}


	expandParents(node: TodoItemFlatNode) {
		const parent = this.getParent(node);
		this.treeControl.expand(parent);
		if (parent && parent.level > 0) {
			this.expandParents(parent);
		}
	}

	getParent(node: TodoItemFlatNode) {
		const { treeControl } = this;
		const currentLevel = treeControl.getLevel(node);
		if (currentLevel < 1) {
			return null;
		}
		const startIndex = treeControl.dataNodes.indexOf(node) - 1;
		for (let i = startIndex; i >= 0; i--) {
			const currentNode = treeControl.dataNodes[i];
			if (treeControl.getLevel(currentNode) < currentLevel) {
				return currentNode;
			}
		}
	}

	logNode(node) {
		let parent: TodoItemFlatNode | null = this.getParentNode(node);
		this.catObj.cat_name = parent.item;
		this.catObj.subcat_name = node.item;
		this.router.navigate(['/dashboard'], { queryParams: this.catObj });		
	}

	onSelectCategory(node) {
		this.userService.selectCategory(this.categoryIdList[node.item]);
	}

	getCategories() {
		this.userService.getAllCategories().subscribe((data: any) => {
			if (data.success === false) {
				this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
			if (data.success === true) {
				this.categoryIdList = data.categoryIdList;
				this.TREE_DATA = data.category;
				this.initialize(this.TREE_DATA);
			}
			if (data[0]) {
				this.snack.open(data[0], 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		});
	}

	openDialogPayment(user, plans) {
		this.dialogRef = this.dialog.open(DialogComponent, {
			width: '1200px',
			height: '900px',
			disableClose: true,
			data: this.dialogConfig.data = {
				paymentStatus: true,
				paymentData: user,
				plans: plans
			}
		});
	}

	openAlertForPayment(days, date) {
		this.dialogRef = this.dialog.open(DialogComponent, {
			width: '1000px',
			height: 'auto',
			disableClose: false,
			data: this.dialogConfig.data = {
				expiryStatus: true,
				remainingDays: days,
				expiryDate: date
			}
		});
	}
}
