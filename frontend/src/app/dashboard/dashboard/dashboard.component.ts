import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material';
import { environment } from "../../../environments/environment";
import { DomSanitizer } from '@angular/platform-browser';
import { zoomInAnimate } from 'src/app/app.animation';
import { SocketService } from "../../services/socket.service";
import { DialogComponent } from 'src/app/dialog/dialog.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [zoomInAnimate]
})
export class DashboardComponent implements OnInit {
  // @ViewChild('videoPlayer', {static: false}) videoplayer: any;

  imgArr: any;
  videoArr: any;
  audioArr: any;
  articleArr: any;
  appurl = '';
  imageEndPoint = '';
  msaapDisplayVolumeControl:boolean = true;

  textflag = false;
  buttonflag = true;

  base64File: string = null;
  filename: string = null;
  base64videoFile: string = null;
  videofilename: string = null;
  indexnum: any;

  image_file: File = null;
  fileerror = [];
  error = [];
  fileupload = [];

  dialogRef: MatDialogRef<DialogComponent>;
  dialogConfig = new MatDialogConfig();


  articleRead = false;
  imgShow = false;
  private subscription: Subscription;

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private snack: MatSnackBar,
    private socketService: SocketService,
    public dialog: MatDialog,
    protected sanitized: DomSanitizer
    ) {
    if (environment.production) {
      this.appurl = environment.apiEndPoint;
      this.imageEndPoint = environment.imageEndPoint;
    } else {
      this.appurl = environment.apiEndPoint;
      this.imageEndPoint = environment.imageEndPoint;
    }
  }

  ngOnInit() {
    this.getUploads();
    this.socketService.updateContentData().subscribe((data) => {
      if (data) {
        this.getUploads();
      }
    });

    this.subscription = this.userService.onSelectCategory.subscribe(response => {
      if(response){
      
      this.router.navigate(['/dashboard']);
      this.userService.getContentById(response).subscribe((success) => {
        if (success.status === true) {
          this.imgArr = success.image;
          this.videoArr = success.video;
          this.articleArr = success.article;
          this.audioArr = success.audio;
          // this.updatedContent();
        }
        if (success.status === false) {
          this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
        }
      }, error => {
        this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      });
    }
      
		});
  }

  logout() {
    this.apiService.logout();
    this.snack.open(
      'You are logged out successfully!', 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
    this.router.navigate(['']);
  }


  getUploads() {
    this.userService.adminUploads().subscribe((success) => {
      if (success.status === true) {
        this.imgArr = success.image;
        this.videoArr = success.video;
        this.articleArr = success.article;
        this.audioArr = success.audio;
        // this.updatedContent();
      }
      if (success.status === false) {
        this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  onFileSelect(e: any, index): void {
    try {
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.filename = file.name;
        this.base64File = _event.target.result;
        this.indexnum = index;
      }
      var filelist = e.target.files;
      for (var i = 0; i < filelist.length; i++) {
        if (!this.validateImageFile(filelist[i].name) || (filelist[i].size > 5000000)) {  // file size not exceeding more than 5 MB 
          this.error['content_url'] = "";
          this.fileerror['content_url'] = "Invalid File ";
          e.target.value = null;
        }
        else {
          this.fileerror['content_url'] = "";
          this.error['content_url'] = "";
          this.fileupload.push(filelist[i]);
        }
      }
    } catch (error) {
      this.filename = null;
      this.base64File = null;
      console.log('no file was selected...');
    }
  }
//   toggleVideo(event: any) {
//     this.videoplayer.nativeElement.play();
// }
// pauseVideo(event:any){
//   this.videoplayer.nativeElement.pause();
//   // this.videoplayer.nativeElement.load();

//   this.videoplayer.nativeElement.setDuration = 0;
// }
  validateImageFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'png') {
      return true;
    }
    else {
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }


  onVideoFileSelect(e: any, index): void {
    try {
      const file = e.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.videofilename = file.name;
        this.base64videoFile = _event.target.result;
        this.indexnum = index;
      }
      var filelist = e.target.files;
      for (var i = 0; i < filelist.length; i++) {
        if (!this.validateVideoUpload(filelist[i].name) || (filelist[i].size > 50000000)) {  // file size not exceeding more than 50 MB 
          this.error['content_url'] = "";
          this.fileerror['content_url'] = "Invalid File ";
          e.target.value = null;
        }
        else {
          this.fileerror['content_url'] = "";
          this.error['content_url'] = "";
          this.fileupload.push(filelist[i]);
        }
      }
    } catch (error) {
      this.videofilename = null;
      this.base64videoFile = null;
      console.log('no file was selected...');
    }
  }

  validateVideoUpload(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'mp4' || ext.toLowerCase() == 'mkv' || ext.toLowerCase() == 'webm' || ext.toLowerCase() == 'wmv') {
      return true;
    }
    else {
      this.snack.open('File extension not permitted.', 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      return false;
    }
  }

  safeHtml(html) {
    return this.sanitized.bypassSecurityTrustHtml(html);
  }

  showAlert(text: string, title: string){
    this.openArticleDialog(text, title);
  }

  openArticleDialog(text, title) {
    this.dialogRef = this.dialog.open(DialogComponent, {
      width: '1200px',
      height: 'auto',
      disableClose: true,
      data: this.dialogConfig.data = {
        articleRead: true,
        articleData : text,
        articletitle : title,
      }
    });
  }


  showimgAlert(img: any){
    this.openImageDialog(img);
  }


  openImageDialog(img) {
    this.dialogRef = this.dialog.open(DialogComponent, {
      width: '1200px',
      height: 'auto',
      disableClose: true,
      data: this.dialogConfig.data = {
        imgShow: true,
        imgPop : img,
      }
    });
  }

  // updatedContent() {
  //   this.socketService.updateContentData().subscribe((data) => {
  //     if (data) {
  //       this.getUploads();
  //     }
  //   });
  // }
//   toggleVideo(event: any, index) {
//     console.log("index",index);
//     this.indexnum = index;
//     if (this.indexnum) {
//       this.videoplayer.nativeElement.play();
//     }
// }
// pauseVideo(event:any, index){
//   console.log("pauseVideo index",index);
//   this.indexnum = null;
//   if (index) {
//     this.videoplayer.nativeElement.pause();
//     // this.videoplayer.nativeElement.load();
//     this.videoplayer.nativeElement.setDuration = 0;
//   }
// }

}



