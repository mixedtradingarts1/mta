import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";
import { ApiService } from "../../services/api.service";
import { ProfileService } from "../../services/profile.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';

@Component({
  selector: 'app-login-logs',
  templateUrl: './login-logs.component.html',
  styleUrls: ['./login-logs.component.css']
})
export class LoginLogsComponent implements OnInit {

  displayedColumns: string[] = ['type', 'ipaddress', 'macaddress', 'browser', 'description', 'createdAt'];
  dataSource = new MatTableDataSource();
  pageIndex = 0;
  pageLimit = [5, 10, 15];
  limitUser = 10;
  totalUserLength = 0;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private profileService: ProfileService,
    private snack: MatSnackBar,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getloginlogs();
  }

  getloginlogs() {
    this.userService.getloginlogs(this.pageIndex, this.limitUser).subscribe((data) => {
      if (data.success === false) {
        this.snack.open(data.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
      }
      if (data.success === true) {
        if (data) {
          this.dataSource.data = data.logs;
          this.totalUserLength = data.count;
        }
      }
    }, error => {
      this.snack.open(error.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
    });
  }

  // chnage limit and previous next button in Users
  getAlllogs(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.limitUser = event.pageSize;
    this.getloginlogs();
  }

}
