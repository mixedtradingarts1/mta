import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from "../../services/user.service";
import { MatSnackBar, MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { ApiService } from "../../services/api.service";
import { environment } from "../../../environments/environment";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-content',
	templateUrl: './content.component.html',
	styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
	id: string;
	appurl = ''
	img: any;
	video: any;
	vid_thumb: any;
	audio: any;
	aud_thumb: any;
	art_thumb: any;
	article: any;
	title: any;
	show = false;
	showReport = false;
	commentObj = {
		comment: '',
		content_id: ''
	}
	comments: any;
	reportObj = {
		text: '',
		content_id: '',
		comment_id: ''
	}

	index: any;
	recordCount = 0;
	limitComment: any
	limit = 5;

	showmoreFlag = true;
	dataloadFlag = false;

	constructor(private activatedRoute: ActivatedRoute,
		private userService: UserService,
		private snack: MatSnackBar,
		private router: Router,
		private apiService: ApiService,
		protected sanitized: DomSanitizer) {
		if (environment.production) {
			this.appurl = environment.apiEndPoint;
		} else {
			this.appurl = environment.apiEndPoint;
		}

	}

	ngOnInit() {
		this.id = this.activatedRoute.snapshot.params.id;
		if (this.id) {
			this.getContentByID(this.id);
			this.getComments();
		}

	}

	getContentByID(id) {
		this.userService.getContentById(id).subscribe((success) => {

			if (success.status === true) {
				if (success.content) {
					if (success.content.contenttype === 'Image') {
						this.img = success.content.content_url;
					}
					else if (success.content.contenttype === 'Video') {
						this.video = success.content.content_url;
						this.vid_thumb = success.content.video_thumbnail;
						this.title = success.content.video_title;
					}

					else if (success.content.contenttype === 'Audio') {
						this.audio = success.content.content_url;
						this.aud_thumb = success.content.audio_thumbnail;
						this.title = success.content.audio_title;

					}
					else if (success.content.contenttype === 'Article') {
						this.article = success.content.article;
						this.art_thumb = success.content.article_thumbnail;
						this.title = success.content.article_title;
					}
				}
			}
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		});
	}

	safeHtml(html) {
		return this.sanitized.bypassSecurityTrustHtml(html);
	}
	safeComment(html) {
		return this.sanitized.bypassSecurityTrustStyle(html);
	}

	// Method toggle comments section
	togglecommnt() {
		if (this.show) {
			this.show = false;
		} else {
			this.show = true;
		}
	}

	togglereport(i: any) {
		this.index = i;
		if (this.showReport) {
			this.showReport = false;
		} else {
			this.showReport = true;
		}
	}

	forReport(text, id) {
		this.reportObj.content_id = this.id;
		this.reportObj.comment_id = id;
		this.reportObj.text = text;
		this.userService.reportComment(this.reportObj).subscribe((success) => {

			if (success.status === true) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				this.getComments();
			}
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		});
	}

	onEnter(comment) {
		this.commentObj.content_id = this.id;
		this.commentObj.comment = comment;
		this.userService.addCommentsOnContents(this.commentObj).subscribe((success) => {
			if (success.status === true) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['info-snackbar'], horizontalPosition: 'end' });
				this.getComments();
			}
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		});

	}

	getComments() {
		this.showmoreFlag = true;
		var limitComment = this.limit + this.recordCount;
		this.userService.getAllCommentsById(this.id, limitComment).subscribe((success) => {
			if (success.status === true) {
				if (success.comments.length > 0) {
					this.comments = success.comments;
					this.comments = this.comments.sort(function(first, second){
						return first.comment.createdAt - second.comment.createdAt;
					});
					if (this.recordCount === success.comments.length || success.comments.length < this.limit) {
						this.showmoreFlag = false;
					}
					this.recordCount = success.comments.length;
				} else {
					this.showmoreFlag = false;
				}
			} else {
				this.showmoreFlag = false;
			}
			this.dataloadFlag = true;
			if (success.status === false) {
				this.snack.open(success.message, 'X', { duration: 4000, panelClass: ['error-snackbar'], horizontalPosition: 'end' });
			}
		});
	}

	moreComments(flag) {
		if (flag) {
			this.recordCount = 0;
		}
		this.getComments();
	}

}
